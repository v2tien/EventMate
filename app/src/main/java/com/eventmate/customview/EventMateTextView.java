package com.eventmate.customview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class EventMateTextView extends TextView{
    public EventMateTextView(Context context) {
        super(context);
        setFont();
    }

    public EventMateTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }

    public EventMateTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont();
    }

    private void setFont(){
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/event_mate_font.OTF");
        setTypeface(font, Typeface.BOLD);
        setAllCaps(false);
    }
}
