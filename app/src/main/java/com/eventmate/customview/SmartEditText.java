package com.eventmate.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import com.eventmate.echelon.HomeActivity;

/**
 * Created by ADMIN on 4/2/2016.
 */
public class SmartEditText extends EditText{
    public SmartEditText(Context context) {
        super(context);
    }

    public SmartEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SmartEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if(event.getKeyCode()==KeyEvent.KEYCODE_BACK){
            setVisibility(View.GONE);
            ((HomeActivity)getContext()).getSearchView().setVisibility(VISIBLE);
            return true;
        }
        return super.onKeyPreIme(keyCode, event);
    }
}
