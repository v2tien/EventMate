package com.eventmate.utils;

import android.content.Context;

/**
 * Created by VuVan on 28/03/2016.
 */
public class Common {
    public static Context context;
    public static int userId = 0;
    public static String name;
    public static String token;
    public static int appStatus = 0;
    public static String senderId = "375834809414";

    public static String publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvVdRqdpV4Pv4rDc9QI07\n" +
        "A4fjUL/kHKvkxaQmE/Dd2ca3nfwxYul1BCRLn5NBw/Qll2SjB/rG9/zEiOEEXZ6L\n" +
        "y/UAxGo+I/YP+quxvYUEzI6uLymh39l9Mo1i0xJyRQSE6B/hQsAEjfpoNhp1MjUi\n" +
        "CBheKCEL6Px2RUNvM0NHKXKj2qIu0lLnw3qjhtkG0bk6e4kbEgq1nNz1w7PbdcLM\n" +
        "cffl2KjSRZ36k0XvhcX7ADcDnaZyR22lDyJ1ZF4ujNu5idp/cU7f74bGAWWuC+n3\n" +
        "U5U5C0OLmCh0HXtH1WHQlSi7vUMctva4OIVvYYBQHLV5CUsPC68xWc4uGqwa08C8\n" +
        "QwIDAQAB";
    public static String appId = "ef7c0a7a8771a46fa201c212977a7dde";
}
