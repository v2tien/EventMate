package com.eventmate.utils;

import java.net.InetSocketAddress;
import java.util.ArrayList;

/**
 * Created by VuVan on 28/03/2016.
 */
public class NetworkCommon {

    public static String email = "";
    public static String phoneNumber = "";
    public static String password = "";
    public static String phoneToken = "";
    public static ArrayList<InetSocketAddress> listMasterOnChatServer = new ArrayList<InetSocketAddress>();
    public static final String UPLOAD_BASE_URL = Constant.Url.URL_BASE + "uploadphototmp/";

    public static void initAddr() {
        listMasterOnChatServer.clear();
//        listMasterOnChatServer.add(new InetSocketAddress("test.azstack.com", 2888));
        listMasterOnChatServer.add(new InetSocketAddress("emma.azstack.com", 2888));
    }
}
