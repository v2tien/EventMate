package com.eventmate.utils;

import android.util.Log;

public class LogUtils {

    private static final boolean CAN_LOG = true;
    private static final String TAG = "AZSTACK";
    private static final String DEFAULT_LOG = "null";

    public static void logInfo(String msg) {
        if (CAN_LOG) {
            if (msg != null) {
                Log.i(TAG, msg);
            } else {
                Log.i(TAG, DEFAULT_LOG);
            }
        }
    }

    public static void logDebug(String msg) {
        if (CAN_LOG) {
            if (msg != null) {
                Log.d(TAG, msg);
            } else {
                Log.d(TAG, DEFAULT_LOG);
            }
        }
    }

    public static void logError(String msg) {
        if (CAN_LOG) {
            if (msg != null) {
                Log.e(TAG, msg);
            } else {
                Log.e(TAG, DEFAULT_LOG);
            }
        }
    }

}
