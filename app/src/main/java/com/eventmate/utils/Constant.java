package com.eventmate.utils;

public class Constant {

    public static class Key {
        public static final String KEY_CODE = "code";
        public static final String KEY_EVENT_ID = "event_id";
        public static final String KEY_TOKEN= "token";
        public static final String KEY_FALSE= "false";
        public static final String KEY_SESSION_ID= "session_id";
        public static final String KEY_SUCCESS= "success";
        public static final String KEY_DATA= "data";
        public static final String KEY_MESSAGE= "message";
        public static final String KEY_TOTAL= "total";
        public static final String KEY_CONTACT_TYPE_ID= "contact_type_id";
        public static final String KEY_TYPE= "type";
        public static final String KEY_RECORD_ID= "record_id";
        public static final String KEY_EMAIL = "email";
        public static final String KEY_URL = "url";
        public static final String KEY_IS_TREE = "is_tree";
        public static final String USER_ID = "user_id";
        public static final String CONTACT_ID = "contact_id";
        public static final String PURPOSE = "purpose";

        public static final String KEY_CATEGORY_ID = "category_id";
        public static final String KEY_SUB_CATEGORY = "sub_categories";
        public static final String KEY_TITLE = "title";
        public static final String KEY_LOCATION = "location";
        public static final String KEY_CELCIUS = "celcius";
        public static final String KEY_IS_BOOKMARK = "is_bookmark";
        public static final String KEY_IS_RECENTLY = "is_recently";

        public static final int APP_STATUS_CONNECTED = 1;
        public static final int APP_STATUS_UPDATING = 2;
        public static final int APP_STATUS_UPDATING_COMPLETE = 3;
        public static final int APP_STATUS_CONNECTING = 4;
        public static final int APP_STATUS_WAITING = 5;

    }

    public static class Url {
        public static final String URL_BASE = "http://api.eventmateapp.com/api/";
        public static final String URL_LOGIN = URL_BASE + "contact/login";
        public static final String URL_USER_FORGOT_PASSWORD = URL_BASE + "contact/forgot_password";

        public static final String URL_SESSION_GET = URL_BASE + "session/get";
        public static final String URL_SESSION_BROWSE = URL_BASE + "session/browse";
        public static final String URL_GET_CONTACT_TYPE = URL_BASE + "contacttype/browse";
        public static final String URL_GET_ALL_SESSION = URL_BASE + "session/browse";
        public static final String URL_GET_ALL_CONTACT = URL_BASE + "contact/browse";
        public static final String URL_BOOKMARK = URL_BASE + "bookmark/update";
        public static final String URL_GET_SESSION_CATEGORY = URL_BASE + "category/browse";
        public static final String URL_GET_USER_BY_ID = URL_BASE + "contact/get";

        public static final String URL_SESION_RATING = URL_BASE + "rate/update";
        public static final String URL_GET_CUSTOM_MENU = URL_BASE + "custommenu/browse";
        public static final String URL_GET_ANNOUNCEMENT = URL_BASE + "announcement/browse";
        public static final String URL_GET_TEMPERATURE = URL_BASE + "temperature/get";

        public static final String URL_DELETE_BOOKMARK = URL_BASE + "bookmark/delete";
        public static final String URL_UPDATE_INFO = URL_BASE + "contact/update";


    }

}
