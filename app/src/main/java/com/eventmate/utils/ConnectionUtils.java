package com.eventmate.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.azstack.AzStackClient;
import com.eventmate.echelon.EchelonApplication;
import com.eventmate.echelon.LoginActivity;
import com.eventmate.manager.DataStore;
import com.eventmate.models.ContactType;
import com.eventmate.models.Session;
import com.eventmate.models.User;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class ConnectionUtils {


    public static void login(String code, String event_id, JsonHttpResponseHandler handler) {
        RequestParams params = new RequestParams();
        params.put(Constant.Key.KEY_CODE, code);
        if (event_id != null && !event_id.isEmpty()) {
            params.put(Constant.Key.KEY_EVENT_ID, event_id);
        }
        post(Constant.Url.URL_LOGIN, params, handler);
    }

    public static void getSessionCategory(JsonHttpResponseHandler handler) {
        RequestParams params = new RequestParams(Constant.Key.KEY_TOKEN, EchelonApplication.getInstance().getToken());
        get(Constant.Url.URL_GET_SESSION_CATEGORY, params, handler);
    }

    public static void getContactType(String token, JsonHttpResponseHandler handler) {
        RequestParams params = new RequestParams(Constant.Key.KEY_TOKEN, token);
        get(Constant.Url.URL_GET_CONTACT_TYPE, params, handler);
    }

    public static void getAllSession(String token, JsonHttpResponseHandler handler) {
        RequestParams params = new RequestParams(Constant.Key.KEY_TOKEN, token);
        get(Constant.Url.URL_GET_ALL_SESSION, params, handler);
    }

    public static void getAllContact(String token, ContactType contactType, JsonHttpResponseHandler handler) {
        RequestParams params = new RequestParams();
        if (contactType != null) {
            if (contactType.getContact_type_id() != ContactType.DEFAULT_ID && contactType.getContact_type_id() != ContactType.DEFAULT_ID_BOOKMARKED) {
                params.put(Constant.Key.KEY_CONTACT_TYPE_ID, contactType.getContact_type_id());
            }
            if (contactType.getContact_type_id() == ContactType.DEFAULT_ID_BOOKMARKED) {
                params.put(Constant.Key.KEY_IS_BOOKMARK, 1);
            }
        }
        params.put(Constant.Key.KEY_TOKEN, token);
        get(Constant.Url.URL_GET_ALL_CONTACT, params, handler);
    }

    public static void bookmark(String token, String type, String recordId, JsonHttpResponseHandler handler) {
        RequestParams params = new RequestParams(Constant.Key.KEY_TOKEN, token);
        params.put(Constant.Key.KEY_TYPE, type);
        params.put(Constant.Key.KEY_RECORD_ID, recordId);
        post(Constant.Url.URL_BOOKMARK, params, handler);
    }

    public static void getCustomMenu(String token, JsonHttpResponseHandler handler) {
        RequestParams params = new RequestParams(Constant.Key.KEY_TOKEN, token);
        get(Constant.Url.URL_GET_CUSTOM_MENU, params, handler);
    }

    public static void getListAnnouncement(String token, JsonHttpResponseHandler handler) {
        RequestParams params = new RequestParams(Constant.Key.KEY_TOKEN, token);
        get(Constant.Url.URL_GET_ANNOUNCEMENT, params, handler);
    }

    public static void getAllSessionCategory(String token, JsonHttpResponseHandler handler) {
        RequestParams params = new RequestParams(Constant.Key.KEY_TOKEN, token);
        params.put(Constant.Key.KEY_IS_TREE, "1");
        get(Constant.Url.URL_GET_SESSION_CATEGORY, params, handler);
    }

    public static void getAllSessionByCategoty(String token, int catId, JsonHttpResponseHandler handler) {
        RequestParams params = new RequestParams(Constant.Key.KEY_TOKEN, token);
        params.put(Constant.Key.KEY_CATEGORY_ID, catId);
        get(Constant.Url.URL_SESSION_BROWSE, params, handler);
    }

    public static void getTemperature(String token, String location, JsonHttpResponseHandler handler) {
        RequestParams params = new RequestParams(Constant.Key.KEY_TOKEN, token);
        params.put(Constant.Key.KEY_LOCATION, location);
        get(Constant.Url.URL_GET_TEMPERATURE, params, handler);

    }

    public static void getUserById(String token, String userId, JsonHttpResponseHandler handler) {
        RequestParams params = new RequestParams(Constant.Key.KEY_TOKEN, token);
        params.put(Constant.Key.CONTACT_ID, userId);
        get(Constant.Url.URL_GET_USER_BY_ID, params, handler);

    }

    public static void getCurrentSession(String token, JsonHttpResponseHandler handler) {
        RequestParams params = new RequestParams(Constant.Key.KEY_TOKEN, token);
        params.put(Constant.Key.KEY_IS_RECENTLY, 1);
        get(Constant.Url.URL_SESSION_BROWSE, params, handler);
    }

    public static void getBookmarkedContact(String token, JsonHttpResponseHandler handler) {
        RequestParams params = new RequestParams(Constant.Key.KEY_TOKEN, token);
        params.put(Constant.Key.KEY_IS_BOOKMARK, 1);
        get(Constant.Url.URL_GET_ALL_CONTACT, params, handler);
    }

    public static void getBookmarkedSession(String token, JsonHttpResponseHandler handler) {
        RequestParams params = new RequestParams(Constant.Key.KEY_TOKEN, token);
        params.put(Constant.Key.KEY_IS_BOOKMARK, 1);
        get(Constant.Url.URL_SESSION_BROWSE, params, handler);
    }

    public static void unBookmarkSession(String token, Session session, JsonHttpResponseHandler handler) {
        RequestParams params = new RequestParams(Constant.Key.KEY_TOKEN, token);
        params.put(Constant.Key.KEY_RECORD_ID, session.getSession_id());
        params.put(Constant.Key.KEY_TYPE, 2);
        get(Constant.Url.URL_DELETE_BOOKMARK, params, handler);
    }

    public static void unBookmarkContact(String token, User user, JsonHttpResponseHandler handler) {
        RequestParams params = new RequestParams(Constant.Key.KEY_TOKEN, token);
        params.put(Constant.Key.KEY_RECORD_ID, user.getContact_id());
        params.put(Constant.Key.KEY_TYPE, 1);
        get(Constant.Url.URL_DELETE_BOOKMARK, params, handler);
    }

    public static void updateInfo(String token, String firstName, String lastName, String mobile, String email, String company, String website, String facebook, String linkedin, String twitter,
                                  String decodedImage, String imageType, JsonHttpResponseHandler handler) {
        RequestParams params = new RequestParams(Constant.Key.KEY_TOKEN, token);
        if(!firstName.isEmpty()) {
            params.put("first_name", firstName);
        }
        if(!lastName.isEmpty()) {
            params.put("last_name", firstName);
        }
        if(!mobile.isEmpty()) {
            params.put("cell_phone", firstName);
        }
        if(!firstName.isEmpty()) {
            params.put("first_name", firstName);
        }
        if(!email.isEmpty()) {
            params.put("email", firstName);
        }
        if(!company.isEmpty()) {
            params.put("company", firstName);
        }
        if(!website.isEmpty()) {
            params.put("website", firstName);
        }
        if(!facebook.isEmpty()) {
            params.put("url_facebook", firstName);
        }
        if(!twitter.isEmpty()) {
            params.put("url_twitter", firstName);
        }
        if(!linkedin.isEmpty()) {
            params.put("url_linkedin", firstName);
        }

        if(!decodedImage.isEmpty()) {
            params.put("image_base64", firstName);
        }

        if(!imageType.isEmpty()){
            params.put("image_type", firstName);
        }

        post(Constant.Url.URL_UPDATE_INFO, params, handler);
    }

    public static void loginFB(String firstName, String lastName, String email, String facebook,
                                  String avatar, JsonHttpResponseHandler handler) {
        RequestParams params = new RequestParams();

        if(!firstName.isEmpty()) {
            params.put("first_name", firstName);
        }
        if(!lastName.isEmpty()) {
            params.put("last_name", lastName);
        }

        if(!avatar.isEmpty()) {
            params.put("img", avatar);
        }

        if(!email.isEmpty()) {
            params.put("email", email);
        }

        if(!facebook.isEmpty()) {
            params.put("url_facebook", facebook);
        }

        params.put("contact_type_id", 38);
        params.put("event_id", 24);

        post(Constant.Url.URL_UPDATE_INFO, params, handler);
    }


    public static boolean checkConnection(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    public static void get(String url, RequestParams params,
                           JsonHttpResponseHandler handler) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(7000);
        client.get(url, params, handler);
    }

    public static void delete(String url, RequestParams params,
                              JsonHttpResponseHandler handler) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(7000);
        client.delete(url, params, handler);
    }

    public static void post(String url, RequestParams params,
                            JsonHttpResponseHandler handler) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(7000);
        client.post(url, params, handler);
    }

    public static void logOut(Activity activity){
        EchelonApplication.getInstance().setToken("");
        AzStackClient.getInstance().logout();
        DataStore.getInstance().clearLoginData();
        DataStore.getInstance().clearLoginCode();
        DataStore.getInstance().logout();
        Intent intent = new Intent(activity, LoginActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }

}
