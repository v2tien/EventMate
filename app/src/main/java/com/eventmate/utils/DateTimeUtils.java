package com.eventmate.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Trang on 3/25/2016.
 */
public class DateTimeUtils {

    public static final String FORMAT_DATETIME = "yyyy-MM-dd HH:mm:ss";

    public static String formatDate(String datetime) {
        DateFormat inputFormatter1 = new SimpleDateFormat(FORMAT_DATETIME);
        Date date1 = null;
        try {
            date1 = inputFormatter1.parse(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        DateFormat outputFormatter1 = new SimpleDateFormat("MMM dd yyyy");
        return outputFormatter1.format(date1);
    }

    public static String formatHour(String datetime) {
        DateFormat inputFormatter1 = new SimpleDateFormat(FORMAT_DATETIME);
        Date date1 = null;
        try {
            date1 = inputFormatter1.parse(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        DateFormat outputFormatter1 = new SimpleDateFormat("KK:MM a ");
        return outputFormatter1.format(date1);
    }

}
