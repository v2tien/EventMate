package com.eventmate.manager;

import android.content.Context;
import android.content.SharedPreferences;

import com.eventmate.models.User;
import com.eventmate.utils.Common;
import com.google.gson.Gson;

import org.json.JSONObject;

/**
 * Created by Trang on 1/8/2016.
 */
public class DataStore {

    private static final String PREF_USER = "PREF_USER";
    private static final String PREF_APP_PREFERENCE = "PREF_APP_PREFERENCE";
    private static final String PREF_SESSION_NOTIFY_ID= "PREF_SESSION_NOTIFY_ID";
    private static final String PREF_SESSION_BOOKMARK_ID = "PREF_SESSION_BOOKMARK_ID";
    private static final String PREF_SESSION_RATING = "PREF_SESSION_RATING";

    private static DataStore instance;
    private static Context mContext;


    public static DataStore getInstance() {
        if (instance == null) {
            throw new RuntimeException("DataStore has not initialized!");
        }
        return instance;
    }

    public static void init(Context ctx) {
        if (instance == null) {
            instance = new DataStore();
            mContext = ctx;
        }
    }

    // ========= save settings notify for sessions
    public void saveNotify(String sessionId, boolean state){
        putBooleanValue(PREF_SESSION_NOTIFY_ID+sessionId,state);
    }

    public boolean getNotifySetting(String sessionId){
        return getBooleanValue(PREF_SESSION_NOTIFY_ID+sessionId);
    }

    // ========== save settings bookmark for sessions
    public void saveBookmark(String sessionId, boolean state){
        putBooleanValue(PREF_SESSION_BOOKMARK_ID+sessionId,state);
    }

    public boolean getBookmark(String sessionId){
        return getBooleanValue(PREF_SESSION_BOOKMARK_ID+sessionId);
    }

    // ========== save user rating
    public void saveRating(String sessionId, float rating){
        putFloatValue(PREF_SESSION_RATING +sessionId,rating);
    }

    public float getRating(String sessionId){
        return getFloatValue(PREF_SESSION_RATING+sessionId);
    }

    // ========== save and get user's info ======================
    public void saveUser(User user) {
        if (user != null) {
            String jsonUser = user.toJSon();
            putStringValue(PREF_USER, jsonUser);
        }
    }

    public User getUser() {
        String jsonUser = getStringValue(PREF_USER);
        User user = new Gson().fromJson(jsonUser, User.class);
        return user;
    }



    /**
     * Save a long integer to MySharedPreferences
     *
     * @param key
     * @param n
     */
    public void putLongValue(String key, long n) {
        // SmartLog.log(TAG, "Set long integer value");
        SharedPreferences pref = mContext.getSharedPreferences(
                PREF_APP_PREFERENCE, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putLong(key, n);
        editor.commit();
    }

    /**
     * Read a long integer to MySharedPreferences
     *
     * @param key
     * @return
     */
    public long getLongValue(String key) {
        // SmartLog.log(TAG, "Get long integer value");
        SharedPreferences pref = mContext.getSharedPreferences(
                PREF_APP_PREFERENCE, 0);
        return pref.getLong(key, 0);
    }

    /**
     * Save an integer to MySharedPreferences
     *
     * @param key
     * @param n
     */
    public void putIntValue(String key, int n) {
        // SmartLog.log(TAG, "Set integer value");
        SharedPreferences pref = mContext.getSharedPreferences(
                PREF_APP_PREFERENCE, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(key, n);
        editor.commit();
    }

    /**
     * Read an integer to MySharedPreferences
     *
     * @param key
     * @return
     */
    public int getIntValue(String key) {
        // SmartLog.log(TAG, "Get integer value");
        SharedPreferences pref = mContext.getSharedPreferences(
                PREF_APP_PREFERENCE, 0);
        return pref.getInt(key, 0);
    }

    /**
     * Save an string to MySharedPreferences
     *
     * @param key
     * @param s
     */
    public void putStringValue(String key, String s) {
        // SmartLog.log(TAG, "Set string value");
        SharedPreferences pref = mContext.getSharedPreferences(
                PREF_APP_PREFERENCE, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, s);
        editor.commit();
    }

    /**
     * Read an string to MySharedPreferences
     *
     * @param key
     * @return
     */
    public String getStringValue(String key) {
        // SmartLog.log(TAG, "Get string value");
        SharedPreferences pref = mContext.getSharedPreferences(
                PREF_APP_PREFERENCE, 0);
        return pref.getString(key, "");
    }

    /**
     * Read an string to MySharedPreferences
     *
     * @param key
     * @param defaultValue
     * @return
     */
    public String getStringValue(String key, String defaultValue) {
        // SmartLog.log(TAG, "Get string value");
        SharedPreferences pref = mContext.getSharedPreferences(
                PREF_APP_PREFERENCE, 0);
        return pref.getString(key, defaultValue);
    }

    /**
     * Save an boolean to MySharedPreferences
     *
     * @param key
     * @params
     */
    public void putBooleanValue(String key, Boolean b) {
        // SmartLog.log(TAG, "Set boolean value");
        SharedPreferences pref = mContext.getSharedPreferences(
                PREF_APP_PREFERENCE, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, b);
        editor.commit();
    }

    /**
     * Read an boolean to MySharedPreferences
     *
     * @param key
     * @return
     */
    public boolean getBooleanValue(String key) {
        // SmartLog.log(TAG, "Get boolean value");
        SharedPreferences pref = mContext.getSharedPreferences(
                PREF_APP_PREFERENCE, 0);
        return pref.getBoolean(key, false);
    }

    /**
     * Save an float to MySharedPreferences
     *
     * @param key
     * @params
     */
    public void putFloatValue(String key, float f) {
        SharedPreferences pref = mContext.getSharedPreferences(
                PREF_APP_PREFERENCE, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putFloat(key, f);
        editor.commit();
    }

    /**
     * Read an float to MySharedPreferences
     *
     * @param key
     * @return
     */
    public float getFloatValue(String key) {
        SharedPreferences pref = mContext.getSharedPreferences(
                PREF_APP_PREFERENCE, 0);
        return pref.getFloat(key, 0.0f);
    }

    public void saveLoginCode(String code){
        SharedPreferences pref = mContext.getSharedPreferences(
                PREF_USER, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("code", code);
        editor.commit();
    }

    public void clearLoginCode(){
        SharedPreferences pref = mContext.getSharedPreferences(
                PREF_USER, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("code", "");
        editor.commit();
    }

    public String getLoginCode(){
        SharedPreferences pref = mContext.getSharedPreferences(
                PREF_USER, 0);
        return pref.getString("code", "");
    }

    public void saveLoginData(JSONObject data){
        SharedPreferences pref = mContext.getSharedPreferences(
                PREF_USER, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("login_data", data.toString());
        editor.commit();
    }

    public String getLoginData(){
        SharedPreferences pref = mContext.getSharedPreferences(
                PREF_USER, 0);
        return pref.getString("login_data", "");
    }

    public void clearLoginData(){
        SharedPreferences pref = mContext.getSharedPreferences(
                PREF_USER, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("login_data", "");
        editor.commit();
    }

    public void saveUserName(String name){
        SharedPreferences pref = mContext.getSharedPreferences(
                PREF_USER, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("user_name", name);
        editor.commit();
    }

    public String getUserName(){
        SharedPreferences pref = mContext.getSharedPreferences(
                PREF_USER, 0);
        return pref.getString("user_name", "");
    }

    public void setUserAvatar(String url){
        SharedPreferences pref = mContext.getSharedPreferences(
                PREF_USER, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("avatar", url);
        editor.commit();
    }

    public String getUserAvatar(){
        SharedPreferences pref = mContext.getSharedPreferences(
                PREF_USER, 0);
        return pref.getString("avatar", "");
    }

    public void logout(){
        SharedPreferences pref = mContext.getSharedPreferences(
                PREF_USER, 0);
        SharedPreferences.Editor editor = pref.edit();
        Common.userId = 0;
        editor.clear();
        editor.commit();

    }
}
