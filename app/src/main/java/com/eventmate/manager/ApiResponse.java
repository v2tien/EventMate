package com.eventmate.manager;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * created by Trang Pham
 *
 */

public class ApiResponse {
    public static final String KEY_NAME_ERROR = "error";
    public static final String KEY_NAME_CODE = "code";
    public static final String KEY_NAME_MESSAGE = "message";
    public static final String KEY_NAME_STATUS = "success";
    public static final boolean KEY_NAME_SUCCESS = true;
    public static final String KEY_NAME_DATA = "data";
    public static final String KEY_NAME_TOTAL = "total";

    public static final int ERROR_CODE_NOERROR = 0;
    public static final int ERROR_CODE_JSON_ERROR = -9001;

    private int code;
    private boolean isError;
    private String message;
    private JSONObject data;
    private String updateLink;

    public ApiResponse(int code, String message) {
        this.setCode(code);
        this.message = message != null ? message : "";
        this.isError = code != ERROR_CODE_NOERROR;
    }

    public ApiResponse(JSONObject json) {
        if (json == null) {
            this.isError = true;
            this.message = "Empty json";
            this.code = ERROR_CODE_JSON_ERROR;
        } else {
            try {
                if (json.getBoolean(KEY_NAME_STATUS) == KEY_NAME_SUCCESS) {
                    this.message = json.optString(KEY_NAME_MESSAGE, "Json success");
                    this.data = json;
                } else {
                    this.isError = true;
                    this.message = json.optString(KEY_NAME_MESSAGE, "Json error");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     *  Method get object from data if data responde is a object
     *  @param Class
     *  @return Object
     *
     */

    public <T> T getResponseObject(Class<T> tClass) {
        JSONObject object = getDataObject();
        T obj = new Gson().fromJson(object.toString(),tClass);
        return obj;
    }

    /**
     *  This get list object from data if responde is array
     *  @param Class
     *  @return List<Object>
     *
     */
    public <T> List<T> getResponseObjectList(Class<T> tClass) {
        List<T> listObj = new ArrayList<>();
        JSONArray arr = getDataArray();
        if (data != null) {
            for (int i = 0; i < arr.length(); i++) {
                JSONObject jo = arr.optJSONObject(i);
                if (jo != null) {
                    T object = new Gson().fromJson(jo.toString(),tClass);
                    listObj.add(object);
                }

            }
        }
        return listObj;
    }

    public JSONObject getRootObject(){
        return this.data;
    }

    public JSONObject getDataObject() {
        return this.data != null ? this.data.optJSONObject(KEY_NAME_DATA) : null;
    }

    public JSONArray getDataArray() {
        return this.data != null ? this.data.optJSONArray(KEY_NAME_DATA) : null;
    }

    public int getResponseTotalObject() {
        return this.data != null ? this.data.optInt(KEY_NAME_TOTAL, 0) : 0;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isError() {
        return this.isError;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }

    public String getUpdateLink() {
        return updateLink;
    }

    public void setUpdateLink(String updateLink) {
        this.updateLink = updateLink;
    }

}
