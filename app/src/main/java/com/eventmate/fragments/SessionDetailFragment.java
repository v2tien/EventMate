package com.eventmate.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.eventmate.adapter.AttendingAdapter;
import com.eventmate.adapter.SpeakerAdapter;
import com.eventmate.echelon.ContactActivity;
import com.eventmate.echelon.EchelonApplication;
import com.eventmate.echelon.R;
import com.eventmate.manager.DataStore;
import com.eventmate.models.Session;
import com.eventmate.models.User;
import com.eventmate.utils.Common;
import com.eventmate.utils.ConnectionUtils;
import com.eventmate.utils.Constant;
import com.eventmate.utils.DateTimeUtils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class SessionDetailFragment extends BaseFragment implements SpeakerAdapter.IListennerSpeakerAdapter, AttendingAdapter.IListennerAttendingAdapter {

    private RatingBar ratingBar;
    private TextView tvDate, tvTimeStart, tvTimeEnd, tvPosition, tvDescription, tvSpeakers, tvAttending;
    private RecyclerView rvSpeakers, rvAttending;

    private List<User> lstSpeakers, lstAttending;
    private SpeakerAdapter speakerAdapter;
    private  AttendingAdapter attendingAdapter;
    private String sessionId;

    private boolean isNeedUpdateRating = false;
    float curRate;

    private RatingBar.OnRatingBarChangeListener ratingBarChangeListener = new RatingBar.OnRatingBarChangeListener() {
        @Override
        public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
            isNeedUpdateRating = true;
            curRate = rating;
        }
    };

    public static SessionDetailFragment newInstance() {
        Bundle args = new Bundle();
        SessionDetailFragment fragment = new SessionDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_session_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    @Override
    public void onDestroy() {
        if (isNeedUpdateRating){
            updateRating();
            DataStore.getInstance().saveRating(sessionId,curRate);
        }
        super.onDestroy();
    }

    @Override
    protected void initView(View view) {
        ratingBar = (RatingBar) view.findViewById(R.id.rb_rating);
        tvSpeakers = (TextView) view.findViewById(R.id.tv_speakers);
        tvAttending = (TextView) view.findViewById(R.id.tv_attending);
        tvDate = (TextView) view.findViewById(R.id.tv_date);
        tvTimeEnd = (TextView) view.findViewById(R.id.tv_end_time);
        tvTimeStart = (TextView) view.findViewById(R.id.tv_start_time);
        tvPosition = (TextView) view.findViewById(R.id.tv_position);
        tvDescription = (TextView) view.findViewById(R.id.tv_description);

        rvSpeakers = (RecyclerView) view.findViewById(R.id.rcv_speakers);
        rvAttending = (RecyclerView) view.findViewById(R.id.rcv_attending);

        rvSpeakers.setHasFixedSize(true);
        rvSpeakers.setLayoutManager(new LinearLayoutManager(self, LinearLayoutManager.HORIZONTAL, false));
        rvAttending.setHasFixedSize(true);
        rvAttending.setLayoutManager(new LinearLayoutManager(self, LinearLayoutManager.HORIZONTAL, false));

        ratingBar.setOnRatingBarChangeListener(ratingBarChangeListener);
    }

    @SuppressLint("StringFormatMatches")
    public void setData(Session data) {
        sessionId = data.getSession_id();
        lstSpeakers = new ArrayList<>();
        lstSpeakers.addAll(data.getModerators());
        if (lstSpeakers.size() > 0)
            lstSpeakers.add(null); // divider
        lstSpeakers.addAll(data.getSpeakers());

        lstAttending = data.getAttendees();

        tvSpeakers.setText(String.format(getString(R.string.session_tv_speakers), lstSpeakers.size()));
        tvAttending.setText(String.format(getString(R.string.session_tv_attending),lstAttending.size()));
        tvDate.setText(DateTimeUtils.formatDate(data.getStart_at()));
        /*tvTimeEnd.setText(DateTimeUtils.formatHour(data.getEnd_at().substring(11,16)));
        tvTimeStart.setText(DateTimeUtils.formatHour(data.getStart_at()).substring(11,16));*/
        tvTimeEnd.setText((data.getEnd_at().substring(11, 16))+" "+data.getM());
        tvTimeStart.setText((data.getStart_at()).substring(11, 16)+" "+data.getM());
        tvPosition.setText(data.getPlace());
        tvDescription.setText(data.getDescription());
        ratingBar.setRating(DataStore.getInstance().getRating(sessionId));

        speakerAdapter = new SpeakerAdapter(self, lstSpeakers, this);
        rvSpeakers.setAdapter(speakerAdapter);

        attendingAdapter = new AttendingAdapter(self, lstAttending, this);
        rvAttending.setAdapter(attendingAdapter);
    }

    private void updateRating() {
        if (sessionId != null) {
            RequestParams params = new RequestParams();
            params.add(Constant.Key.KEY_TOKEN, EchelonApplication.getInstance().getToken());
            params.add(Constant.Key.KEY_SESSION_ID,sessionId );
            if(ConnectionUtils.checkConnection(mActivity)){
                ConnectionUtils.get(Constant.Url.URL_SESION_RATING, params, new JsonHttpResponseHandler(){
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Log.d("updateRating()","success");
                    }
                });
            }else{
                mActivity.makeToast(R.string.network_no);
            }
        }

    }

    private void gotoContact(User user){
        if(Integer.parseInt(user.getContact_id()) != Common.userId) {
            Intent intent = new Intent(self, ContactActivity.class);
            intent.putExtra(ContactActivity.PREF_USER_KEY, user);
            startActivity(intent);
        }
    }

    @Override
    public void onClickSpeaker(int position) {
        User user = lstSpeakers.get(position);
        gotoContact(user);
    }

    @Override
    public void onClickAttending(int position) {
        User user = lstAttending.get(position);
        gotoContact(user);
    }
}
