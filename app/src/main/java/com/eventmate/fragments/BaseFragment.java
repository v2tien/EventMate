package com.eventmate.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.eventmate.echelon.BaseActivity;

public abstract class BaseFragment extends Fragment{

    protected BaseActivity mActivity;
    protected View root;
    protected Context self;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (BaseActivity)getActivity();
        self = getContext();
    }

    protected abstract void initView(View root);
}
