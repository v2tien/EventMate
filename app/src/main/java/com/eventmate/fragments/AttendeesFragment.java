package com.eventmate.fragments;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.eventmate.adapter.ContactAdapter;
import com.eventmate.adapter.ContactTypeAdapter;
import com.eventmate.echelon.ContactActivity;
import com.eventmate.echelon.EchelonApplication;
import com.eventmate.echelon.HomeActivity;
import com.eventmate.echelon.R;
import com.eventmate.models.ContactType;
import com.eventmate.models.User;
import com.eventmate.utils.Common;
import com.eventmate.utils.ConnectionUtils;
import com.eventmate.utils.Constant;
import com.eventmate.utils.DeviceUtils;
import com.eventmate.utils.LogUtils;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import cz.msebera.android.httpclient.Header;

public class AttendeesFragment extends BaseFragment implements ContactAdapter.IContactAdapterListenner, View.OnTouchListener {

    private static AttendeesFragment INSTANCE;
    private ListView lvAttendee;
    private View sideIndex;
    private TextView tvHeader;
    private View layoutContent;
    private ContactType focusContactType;

    private PopupWindow popupWindow;
    private View layoutPopup;
    private int popupWidth, popupHeight, searchViewHeight;
    private int[] popupLocation = new int[2];

    private int sideIndexHeight;
    private int[] positions = new int[26];
    private static final String[] character = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    public static ArrayList<User> listContatcs = new ArrayList<User>();
    public ArrayList<User> listAll = new ArrayList<User>();
    public ContactAdapter mAdapter;
    private boolean firstTime = true;
    public static String filterText = "All attendees";
    private ArrayList<User> listBookmarkedContact = new ArrayList<User>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_attendees, container, false);
        listContatcs = new ArrayList();
        initView(root);
        return root;
    }

    @Override
    protected void initView(View root) {
        lvAttendee = (ListView) root.findViewById(R.id.lv_attendees);
        lvAttendee.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onClickContactItem(position);
            }
        });
        /*lvAttendee.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                LogUtils.logInfo("firstVisibleItem="+firstVisibleItem+";visibleItemCount = "+visibleItemCount+";totalItemCount="+totalItemCount);
                if(mAdapter!=null && firstVisibleItem<mAdapter.getCount()) {
                    User user = mAdapter.getListContact().get(firstVisibleItem);
                    tvHeader.setText(Character.toString(user.getFirst_name().charAt(0)));
                }
            }
        });*/
        tvHeader = (TextView) root.findViewById(R.id.tv_header);
        sideIndex = root.findViewById(R.id.side_index);
        sideIndex.post(new Runnable() {
            @Override
            public void run() {
                sideIndexHeight = sideIndex.getHeight();
                calculate();
            }
        });
        sideIndex.setOnTouchListener(this);
        layoutContent = root.findViewById(R.id.layout_no_content);

    }

    /*public void showPopupSugguestSearch() {
        //String[] menuArr = getResources().getStringArray(R.array.popupsearch);
        PopupMenu pm = null;
        pm = new PopupMenu(mActivity, ((HomeActivity) mActivity).getSearchView());
        for (ContactType contactType : EchelonApplication.getInstance().getListContactType()) {
            pm.getMenu().add(contactType.getTitle());
        }
        pm.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                String title = item.getTitle().toString();

                for (int i = 0; i < EchelonApplication.getInstance().getListContactType().size(); i++) {
                    ContactType contactType = EchelonApplication.getInstance().getListContactType().get(i);
                    if (EchelonApplication.getInstance().getListContactType().get(i).getTitle().equals(title)) {
                        if (contactType.getTitle().equals(title)) {
                            focusContactType = contactType;
                            ((HomeActivity) mActivity).getSearchView().setText(title);
                            //searchContactByType(contactType);
                            break;
                        }
                    }
                }
                //startActivity(new Intent(mActivity, AnnoucementActivity.class));
                return true;
            }
        });
        pm.show();
    }*/

    public static AttendeesFragment getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AttendeesFragment();
        }
        return INSTANCE;
    }

//    public void searchContactByType(ContactType contactType) {
//        if(contactType == null || contactType.getContact_type_id() == ContactType.DEFAULT_ID){
//            filterText = "All attendees";
//        }else{
//            filterText = contactType.getTitle();
//        }
//        ((HomeActivity)mActivity).getSearchView().setText(filterText);
//        if (ConnectionUtils.checkConnection(mActivity)) {
//            mActivity.showProgressDialog(contactType!=null?"Getting contact type " + contactType.getTitle():"Getting contacts");
//            JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
//                @Override
//                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
//                    LogUtils.logInfo("response attendees:"+response.toString());
//                    mActivity.hideProgressDialog();
//                    if(listContatcs == null){
//                        listContatcs = new ArrayList<User>();
//                    }
//                    listContatcs.clear();
//                    try {
//                        boolean success = response.getBoolean(Constant.Key.KEY_SUCCESS);
//                        String message = response.getString(Constant.Key.KEY_MESSAGE);
//                        if (!success) {
//                            mActivity.showAlertMessage(message);
//                        } else {
//                            Gson gson = new Gson();
//                            JSONArray arrData = response.getJSONArray(Constant.Key.KEY_DATA);
//                            for (int i = 0; i < arrData.length(); i++) {
//                                listContatcs.add(gson.fromJson(arrData.getJSONObject(i).toString(), User.class));
//                            }
//                            showContact();
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                @Override
//                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
//                    mActivity.hideProgressDialog();
//                }
//            };
//            ConnectionUtils.getAllContact(EchelonApplication.getInstance().getToken(), contactType, handler);
//        }else{
//            mActivity.makeToast(R.string.network_no);
//        }
//
//    }

    private void searchByCharacter(String character) {
        ArrayList<User> listTemp = new ArrayList<User>();
        for (User user : listContatcs) {
            if (Character.toString(user.getFirst_name().charAt(0)).equalsIgnoreCase(character)) {
                listTemp.add(user);
            }
        }
        /*listContatcs.clear();
        listContatcs.addAll(listTemp);*/
        showContact(listTemp);
    }

    private void showContact(ArrayList<User> listContatcs) {
        Collections.sort(listContatcs, new Comparator<User>() {
            @Override
            public int compare(User lhs, User rhs) {
                return lhs.compare(rhs);
            }
        });
        mAdapter = new ContactAdapter(mActivity, listContatcs, this);
        lvAttendee.setAdapter(mAdapter);
        if (listContatcs.size() > 0) {
            tvHeader.setText(Character.toString(listContatcs.get(0).getFirst_name().charAt(0)));
            firstTime = false;
        }
    }

    public void showContact() {
        if(listContatcs.size() == 0){
            showNoContent();
        }else{
            hideNoContent();
        }
        Collections.sort(listContatcs, new Comparator<User>() {
            @Override
            public int compare(User lhs, User rhs) {
                return lhs.compare(rhs);
            }
        });
        mAdapter = new ContactAdapter(mActivity, listContatcs, this);
        lvAttendee.setAdapter(mAdapter);
        if (listContatcs.size() > 0) {
            //tvHeader.setText(Character.toString(listContatcs.get(0).getFirst_name().charAt(0)));
            firstTime = false;
        }
    }

    /*@Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ContactAdapter adapter = (ContactAdapter) lvAttendee.getAdapter();
        User user = adapter.getListContact().get(position);

        Intent intent = new Intent(mActivity, ContactActivity.class);
        intent.putExtra(ContactActivity.PREF_USER_KEY, user);
        startActivity(intent);
    }*/

    public ContactType getFocusContactType() {
        return focusContactType;
    }

    public void setFocusContactType(ContactType focusContactType) {
        this.focusContactType = focusContactType;
    }

    public void showPopupContactType() {
        final Button searchView = (((HomeActivity) mActivity).getSearchView());
        popupWidth = HomeActivity.searchViewWidth;
        searchViewHeight = HomeActivity.searchViewHeight;
        searchView.getLocationOnScreen(popupLocation);
        ViewGroup viewGroup = (LinearLayout) mActivity.findViewById(R.id.popup);
        LayoutInflater inflater = LayoutInflater.from(mActivity);
        layoutPopup = inflater.inflate(R.layout.menu_select_contact_type, viewGroup);
        popupWindow = new PopupWindow(mActivity);
        popupWindow.setContentView(layoutPopup);
        final ListView lvContactype = (ListView) popupWindow.getContentView().findViewById(R.id.lv_contact_type);
        ContactTypeAdapter adapter = new ContactTypeAdapter(mActivity, EchelonApplication.getInstance().getListContactType());
        lvContactype.setAdapter(adapter);
        lvContactype.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    focusContactType = new ContactType( ContactType.DEFAULT_ID,"All attendees");
                    ((HomeActivity) mActivity).getSearchView().setText("All attendees");
                    tvHeader.setText("All attendees");
                } else if (position == 1) {
                    focusContactType = new ContactType( ContactType.DEFAULT_ID_BOOKMARKED,"Bookmarked");
                    ((HomeActivity) mActivity).getSearchView().setText("Bookmarked");
                    tvHeader.setText("Bookmarked");
                } else {
                    focusContactType = EchelonApplication.getInstance().getListContactType().get(position);
                    ((HomeActivity) mActivity).getSearchView().setText(focusContactType.getTitle());
                    tvHeader.setText(focusContactType.getTitle());
                }
                ((HomeActivity) mActivity).onTabClick(1);
                ((HomeActivity) mActivity).hideDarkBackground();
                ((HomeActivity) mActivity).searchContactByType(focusContactType);
                popupWindow.dismiss();
            }
        });
        popupWindow.setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.white)));
        popupWindow.setWidth(popupWidth);
        //popupWindow.setHeight(popupHeight);
        popupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        popupWindow.setFocusable(true);
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                ((HomeActivity) mActivity).hideDarkBackground();
            }
        });
        LogUtils.logInfo("x=" + popupLocation[0] + ";y=" + popupLocation[1]);
        ((HomeActivity) mActivity).showDarkBackground();
        popupWindow.showAtLocation(layoutPopup, Gravity.NO_GRAVITY, popupLocation[0], popupLocation[1] + searchViewHeight + 5);

    }


    @Override
    public void onClickContactItem(int position) {
        //User user = listContatcs.get(position);
        User user = mAdapter.getItem(position);
        if (Integer.parseInt(user.getContact_id()) != Common.userId) {
            Intent intent = new Intent(mActivity, ContactActivity.class);
            intent.putExtra(ContactActivity.PREF_USER_KEY, user);
            startActivity(intent);
        }
        DeviceUtils.hideKeyboard(mActivity, ((HomeActivity) mActivity).getSearchView2());
    }

    @Override
    public void onClickBookMark(int position) {
        User contact = mAdapter.getItem(position);
        boolean isBookmark = contact.is_bookmark();
        if(isBookmark){
            unBookmark(contact);
        }else{
            bookmark(contact);
        }
//        mActivity.makeToast("click");
        LogUtils.logInfo("Pos = "+position);
        /*contact.setIs_bookmark(true);
        //contact.is_bookmark() == true ? contact.setIs_bookmark(false) : contact.setIs_bookmark(false);
        bookmark(contact.getContact_id());*/
    }

    private void unBookmark(final User contact) {
        if (ConnectionUtils.checkConnection(self)) {
            mActivity.showProgressDialog("Loading...");
            JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    mActivity.hideProgressDialog();
                    try {
                        boolean success = response.getBoolean(Constant.Key.KEY_SUCCESS);
                        String message = response.getString(Constant.Key.KEY_MESSAGE);
                        if(!success) {
                            mActivity.showAlertMessage(message);
                        }else
                        if(success){
                            contact.setIs_bookmark(false);
                        }
                        mAdapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    mActivity.hideProgressDialog();
                    ConnectionUtils.logOut(getActivity());
                }
            };
            ConnectionUtils.unBookmarkContact(EchelonApplication.getInstance().getToken(), contact, handler);
        } else {
            mActivity.makeToast(R.string.network_no);
        }
    }

    private void bookmark(final User contact) {
        if (ConnectionUtils.checkConnection(self)) {
            mActivity.showProgressDialog("Loading...");
            JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    mActivity.hideProgressDialog();
                    try {
                        boolean success = response.getBoolean(Constant.Key.KEY_SUCCESS);
                        String message = response.getString(Constant.Key.KEY_MESSAGE);
                        if(!success) {
                            mActivity.showAlertMessage(message);
                        }else
                        if(success){
                            contact.setIs_bookmark(true);

                        }
                        mAdapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    mActivity.hideProgressDialog();
                    ConnectionUtils.logOut(getActivity());
                }
            };
            ConnectionUtils.bookmark(EchelonApplication.getInstance().getToken(), Integer.toString(1), contact.getContact_id(), handler);
        } else {
            mActivity.makeToast(R.string.network_no);
        }
    }

    //shit code
    private void calculate() {
        int unit = sideIndexHeight / 26;
        for (int i = 0; i < positions.length; i++) {
            positions[i] = unit * i;
        }
    }

    private String getCharByTouch(View v, MotionEvent event) {
        int index = 25;
        float screenY = event.getY();
        int viewY = (int) screenY - v.getTop();
        LogUtils.logInfo("clik alphabet view y = " + viewY);
        for (int i = 0; i < positions.length; i++) {
            if (i < positions.length - 1 && positions[i] < viewY && positions[i + 1] > viewY) {
                index = i;
                break;
            }
        }
        return character[index];
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        String character = getCharByTouch(v, event);
        tvHeader.setText(character);
        searchByCharacter(character);
        return false;
    }

    private void getBookmarkedContact() {
        JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    mActivity.hideProgressDialog();
                    boolean success = response.getBoolean(Constant.Key.KEY_SUCCESS);
                    if (success) {

                        listBookmarkedContact.clear();
                        Gson gson = new Gson();
                        JSONArray arrData = response.getJSONArray(Constant.Key.KEY_DATA);
                        for (int i = 0; i < arrData.length(); i++) {
                            User user = gson.fromJson(arrData.getJSONObject(i).toString(), User.class);
                            listBookmarkedContact.add(user);
                        }
                        mAdapter = new ContactAdapter(mActivity, listBookmarkedContact, AttendeesFragment.this);
                        //
                        filterText = "Bookmarked";
                        ((HomeActivity) mActivity).getSearchView().setText(filterText);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                mActivity.hideProgressDialog();
                ConnectionUtils.logOut(getActivity());
            }
        };
        if (ConnectionUtils.checkConnection(mActivity)) {
            mActivity.showProgressDialog("Loading...");
            ConnectionUtils.getBookmarkedContact(EchelonApplication.getInstance().getToken(), handler);
        } else {
            mActivity.makeToast(R.string.network_no);
        }
    }

    public void searchContactByName(String name){
        ArrayList<User> listResult = new ArrayList<User>();
        for(User user:listAll){
         String fullName = user.getFirst_name()+" "+user.getLast_name();
            if(fullName.toUpperCase().contains(name.toUpperCase())){
                listResult.add(user);
            }
        }
        mAdapter.setListContact(listResult);
        mAdapter.notifyDataSetChanged();
        if(mAdapter.getCount() == 0){
            showNoContent();
        }else{
            hideNoContent();
        }
    }

    public void showAllAttendees(){
        if(listAll.size()>0){
            mAdapter.setListContact(listAll);
            mAdapter.notifyDataSetChanged();
        }
    }

    public void changeHeader(){
        tvHeader.setText(filterText);
    }

    public boolean isFirstTime() {
        return firstTime;
    }

    public void setFirstTime(boolean firstTime) {
        this.firstTime = firstTime;
    }

    public void clearData() {
        firstTime = true;
        listContatcs.clear();

    }

    public String getFilterText() {
        return filterText;
    }

    public void setFilterText(String filterText) {
        this.filterText = filterText;
    }

    public  ArrayList<User> getListAll() {
        return listAll;
    }

    public  void setListAll(ArrayList<User> listAll) {
        this.listAll = listAll;
    }

    public void showNoContent(){
        layoutContent.setVisibility(View.VISIBLE);
    }

    public void hideNoContent(){
        layoutContent.setVisibility(View.GONE);
    }
}
