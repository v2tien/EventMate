package com.eventmate.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eventmate.customview.SmartImageView;
import com.eventmate.echelon.R;
import com.eventmate.utils.MyTextUtils;

public class AtendaFragment extends BaseFragment {

    private SmartImageView ivImage;
    private TextView tvTemp;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_agenda, container, false);
        initView(root);
        return root;
    }

    @Override
    protected void initView(View root) {
        tvTemp = (TextView)root.findViewById(R.id.tv_temp);
        tvTemp.setText(Html.fromHtml("32"+ MyTextUtils.toSup("o")));
    }


}
