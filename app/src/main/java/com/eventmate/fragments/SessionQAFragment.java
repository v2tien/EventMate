package com.eventmate.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.eventmate.echelon.R;
import com.eventmate.utils.LogUtils;

/**
 * Created by Trang on 3/24/2016.
 */
public class SessionQAFragment extends BaseFragment {
    private static final String KEY_URL = "URL";

    private WebView wvContent;
    private String url;


    public static SessionQAFragment newInstance(String url) {
        LogUtils.logInfo("Url="+url);
        Bundle args = new Bundle();
        args.putString(KEY_URL, url);
        SessionQAFragment fragment = new SessionQAFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        url = getArguments().getString(KEY_URL);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_session_qa, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    @Override
    protected void initView(View view) {
        wvContent = (WebView) view.findViewById(R.id.wb_content);
        wvContent.getSettings().setJavaScriptEnabled(true);
        wvContent.getSettings().setDomStorageEnabled(true);
        wvContent.getSettings().setUseWideViewPort(true);
        wvContent.getSettings().setLoadWithOverviewMode(true);
        wvContent.setWebViewClient(new MyBrowser());
        wvContent.loadUrl(url);
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
