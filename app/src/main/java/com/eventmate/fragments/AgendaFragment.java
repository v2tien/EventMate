package com.eventmate.fragments;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.eventmate.adapter.SessionAdapter;
import com.eventmate.adapter.SessionFilterAdapter;
import com.eventmate.echelon.EchelonApplication;
import com.eventmate.echelon.HomeActivity;
import com.eventmate.echelon.R;
import com.eventmate.echelon.SessionDetailActivity;
import com.eventmate.impl.IAdapterListenner;
import com.eventmate.manager.DataStore;
import com.eventmate.models.Session;
import com.eventmate.models.SessionFactory;
import com.eventmate.models.SessionFilter;
import com.eventmate.models.SessionFilterContent;
import com.eventmate.models.SessionFilterHeader;
import com.eventmate.models.SessionTitleIndex;
import com.eventmate.utils.ConnectionUtils;
import com.eventmate.utils.Constant;
import com.eventmate.utils.LogUtils;
import com.eventmate.utils.MyTextUtils;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import cz.msebera.android.httpclient.Header;

public class AgendaFragment extends BaseFragment implements IAdapterListenner {

    private ImageView ivImage;
    private TextView tvTemp, tvLocation;
    private View headerView;
    private ListView lv;
    //private View layoutNoContent;
    private SessionAdapter adapter;

    private ArrayList<Session> listSession = new ArrayList<Session>();
    private ArrayList<Session> listSessionSearch = new ArrayList<Session>();
    private ArrayList<Session> listSessionCurrent = new ArrayList<Session>();
    private ArrayList<Session> listSessionBookmarked = new ArrayList<Session>();
    //ko dung
    private ArrayList<SessionFilter> listFilterByDate;
    private ArrayList<SessionFilter> listFilterByLocation;
    //dung
    private ArrayList<SessionFilter> listFilterAll = new ArrayList<SessionFilter>();
    private ArrayList<SessionFilter> listFilterHeader = new ArrayList<SessionFilter>();
    private ArrayList<SessionFilter> listFilterContent= new ArrayList<SessionFilter>();

    private PopupWindow popupWindow;
    private View layoutPopup;
    private int popupWidth, popupHeight, searchViewHeight;
    private int[] popupLocation = new int[2];

    private String filterText = "All sessions";
    private int focusedPosition;
    public static boolean updateAdapter=  false;

    private static AgendaFragment INSTANCE;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_agenda, container, false);
        initView(root);

        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        if (listSession.size() == 0) {
            loadAllSession();
        }
    }

    @Override
    protected void initView(View root) {
        headerView = LayoutInflater.from(self).inflate(R.layout.layout_header_agenda,null);
        ivImage = (ImageView)headerView.findViewById(R.id.iv_image);
        if(EchelonApplication.getInstance().getUser().getImg_background()!=null && !EchelonApplication.getInstance().getUser().getImg_background().isEmpty()){
            LogUtils.logInfo("getImg_background:"+EchelonApplication.getInstance().getUser().getImg_background());
            Picasso.with(mActivity).load(EchelonApplication.getInstance().getUser().getImg_background()).into(ivImage);
        }
        /*Picasso.with(mActivity).load("https://scontent-hkg3-1.xx.fbcdn.net/hphotos-xfa1/v/t1.0-9/392419_149484658515556_1027666626_n.jpg?oh=a9b711025b7612156cb7905e676711e4&oe=57920E4E").into
                (ivImage);*/
        tvTemp = (TextView) headerView.findViewById(R.id.tv_temp);
        tvTemp.setText(Html.fromHtml("32" + MyTextUtils.toSup("o")));
        tvLocation = (TextView)headerView.findViewById(R.id.tv_location);
        //tvLocation.setText("Singapore");
        lv = (ListView) root.findViewById(R.id.lv_info);
        lv.addHeaderView(headerView);
        //layoutNoContent = root.findViewById(R.id.layout_no_content);
        getTemp();
    }

    private void getAllSessionCategory(){
        JsonHttpResponseHandler handler = new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                mActivity.hideProgressDialog();
                try {
                    boolean success = response.getBoolean(Constant.Key.KEY_SUCCESS);
                    if(success) {
                        listFilterAll.clear();
                        JSONArray arrData = response.getJSONArray(Constant.Key.KEY_DATA);
                        //data fix cung
                        SessionFilterHeader defaultHeader = new SessionFilterHeader(SessionFilterHeader.DEFAULT_ID, "All");
                        SessionFilterContent contentAll = new SessionFilterContent(SessionFilterContent.DEFAULT_ID_ALL, "All sessions");
                        SessionFilterContent contentBookmarked = new SessionFilterContent(SessionFilterContent.DEFAULT_ID_BOOKMARKED, "Bookmarked");
                        SessionFilterContent contentCurrent = new SessionFilterContent(SessionFilterContent.DEFAULT_ID_CURRENT, "Current");
                        listFilterAll.add(defaultHeader);
                        listFilterAll.add(contentAll);
                        listFilterAll.add(contentBookmarked);
                        listFilterAll.add(contentCurrent);

                        //data tu server
                        for (int i = 0; i < arrData.length(); i++) {
                            JSONObject jsonParentCat = arrData.getJSONObject(i);
                            SessionFilterHeader header = new SessionFilterHeader(jsonParentCat.getInt(Constant.Key.KEY_CATEGORY_ID), jsonParentCat.getString(Constant.Key.KEY_TITLE));
                            listFilterAll.add(header);
                            JSONArray jsonParentCatSubArrData = jsonParentCat.getJSONArray(Constant.Key.KEY_SUB_CATEGORY);
                            for (int j = 0; j < jsonParentCatSubArrData.length(); j++) {
                                JSONObject jsonSubCat = jsonParentCatSubArrData.getJSONObject(j);
                                SessionFilterContent content = new SessionFilterContent(jsonSubCat.getInt(Constant.Key.KEY_CATEGORY_ID), jsonSubCat.getString(Constant.Key.KEY_TITLE));
                                listFilterAll.add(content);
                            }
                        }


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                mActivity.hideProgressDialog();
                ConnectionUtils.logOut(getActivity());
            }
        };
        ConnectionUtils.getAllSessionCategory(EchelonApplication.getInstance().getToken(), handler);
    }

    public void loadAllSession() {
        if (ConnectionUtils.checkConnection(mActivity)) {
            mActivity.showProgressDialog("Loading data...");
            JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    mActivity.hideProgressDialog();
                    ConnectionUtils.logOut(getActivity());
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    mActivity.hideProgressDialog();
                    ConnectionUtils.logOut(getActivity());
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    LogUtils.logInfo("session detail:"+response.toString());
                    //mActivity.hideProgressDialog();
                    Gson gson = new Gson();
                    try {
                        boolean success = response.getBoolean(Constant.Key.KEY_SUCCESS);
                        ((HomeActivity)mActivity).getSearchView().setText("All sessions");
                        if (success) {
                            listSession.clear();
                            JSONArray dataArr = response.getJSONArray(Constant.Key.KEY_DATA);
                            for (int i = 0; i < dataArr.length(); i++) {
                                Session session = gson.fromJson(dataArr.getJSONObject(i).toString(), Session.class);
                                listSession.add(session);
                            }
                            //sort seassion by date
                            Collections.sort(listSession, new Comparator<Session>() {
                                @Override
                                public int compare(Session lhs, Session rhs) {
                                    return lhs.compare(rhs);
                                }
                            });
                            addSessionHeader(1);
                            showAllSession();
                            getAllSessionCategory();
                            listFilterByDate = createFilterByDate();
                            listFilterByLocation = createFilterByLocation();
                            //
                            filterText = "All sessions";
                            ((HomeActivity)mActivity).getSearchView().setText(filterText);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            };
            String token = EchelonApplication.getInstance().getToken();
            Log.e("token login", token);
            ConnectionUtils.getAllSession(token, handler);
        }
    }

    private void searchSessionByCat(SessionFilterContent sfc){
        JsonHttpResponseHandler handler = new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                mActivity.hideProgressDialog();
                listSessionSearch = new ArrayList<Session>();
                listSessionSearch.clear();
                try {
                    boolean success = response.getBoolean(Constant.Key.KEY_SUCCESS);
                    if(success){
                        JSONArray arrData = response.getJSONArray(Constant.Key.KEY_DATA);
                        Gson gson = new Gson();
                        for(int i=0;i<arrData.length();i++){
                            Session session = new Session();
                            session.setSession_id(Integer.toString(arrData.getJSONObject(i).getInt("session_id")));
                            session.setTitle(arrData.getJSONObject(i).getString(Constant.Key.KEY_TITLE));
                            session.setStart_at(arrData.getJSONObject(i).getString("start_at"));
                            session.setEnd_at(arrData.getJSONObject(i).getString("end_at"));
                            session.setPlace(arrData.getJSONObject(i).getString("place"));
                            session.setDescription(arrData.getJSONObject(i).getString("description"));
                            session.setMap(arrData.getJSONObject(i).getString("map"));
                            session.setTotal_attendee(arrData.getJSONObject(i).getInt("total_attendee"));
                            session.setTotal_rate(arrData.getJSONObject(i).getInt("total_rate"));
                            session.setIs_bookmark(arrData.getJSONObject(i).getBoolean("is_bookmark"));

                           /* if(i == 0){
                                listSessionSearch.add(new SessionTitleIndex(session.getStart_at().substring(0,10)));
                            }*/
                            listSessionSearch.add(session);
                            /*Session session = gson.fromJson(arrData.getJSONObject(i).toString(), Session.class);
                            if(i == 0){
                                listSessionSearch.add(new SessionTitleIndex(session.getStart_at().substring(0,10)));
                            }
                            listSessionSearch.add(gson.fromJson(arrData.getJSONObject(i).toString(), Session.class));*/
                        }
                        addSessionHeader(2);
                        adapter.setListSession(listSessionSearch);
                        adapter.notifyDataSetChanged();
                        /*if(adapter.getCount() == 0){
                            layoutNoContent.setVisibility(View.VISIBLE);
                        }else{
                            layoutNoContent.setVisibility(View.GONE);
                        }*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                mActivity.hideProgressDialog();
                ConnectionUtils.logOut(getActivity());
            }
        };
        if(ConnectionUtils.checkConnection(mActivity)){
            mActivity.showProgressDialog("Loading...");
            filterText = sfc.getTitle();
            ((HomeActivity)mActivity).getSearchView().setText(filterText);
            ConnectionUtils.getAllSessionByCategoty(EchelonApplication.getInstance().getToken(), sfc.getId(), handler);
        }else{
            mActivity.makeToast(R.string.network_no);
        }
    }

    private void addSessionHeader(int type) { // 1=all, 2=search, 3=bookmarked

        ArrayList<Session> listSessionTemp = new ArrayList<Session>();
        ArrayList<Session> listSessionCopy;
        if(type == 1){
            listSessionCopy = listSession;
        }else if(type == 2){
            listSessionCopy = listSessionSearch;
        }else{
            listSessionCopy = listSessionBookmarked;
        }
        int originSize = listSessionCopy.size();
        for (int i = 0; i < originSize; i++) {
            if (i == 0) {
                Session s = listSessionCopy.get(i);
                listSessionTemp.add(new SessionTitleIndex(s.getStart_at().substring(0, 10)));
                listSessionTemp.add(s);
            } else {
                Session s = listSessionCopy.get(i);
                Session sPrev = listSessionCopy.get(i - 1);
                if (!s.isEqualDate(sPrev)) {
                    listSessionTemp.add(new SessionTitleIndex(s.getStart_at().substring(0, 10)));
                }
                listSessionTemp.add(s);
            }
        }
        if(type ==1){
            listSession = listSessionTemp;
        }else if(type == 2){
            listSessionSearch = listSessionTemp;
        }else{
            listSessionBookmarked = listSessionTemp;
        }
        //EchelonApplication.getInstance().setListSession(listSessionTemp);
    }

    private void addSessionHeaderBookmarked() {

        ArrayList<Session> listSessionTemp = new ArrayList<Session>();
        int originSize = listSession.size();
        for (int i = 0; i < originSize; i++) {
            if (i == 0) {
                Session s = listSession.get(i);
                listSessionTemp.add(new SessionTitleIndex(s.getStart_at().substring(0, 10)));
                listSessionTemp.add(s);
            } else {
                Session s = listSession.get(i);
                Session sPrev = listSession.get(i - 1);
                if (!s.isEqualDate(sPrev)) {
                    listSessionTemp.add(new SessionTitleIndex(s.getStart_at().substring(0, 10)));
                }
                listSessionTemp.add(s);
            }
        }
        listSession = listSessionTemp;
        //EchelonApplication.getInstance().setListSession(listSessionTemp);
    }


    public void showAllSession() {
        adapter = new SessionAdapter(mActivity, listSession, this);
        lv.setAdapter(adapter);
        /*if(adapter.getCount() == 0){
            layoutNoContent.setVisibility(View.VISIBLE);
        }else{
            layoutNoContent.setVisibility(View.GONE);
        }*/
    }



    public static AgendaFragment getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AgendaFragment();
        }
        return INSTANCE;
    }

    private ArrayList<SessionFilter> createFilterByDate(){
        ArrayList<SessionFilter> listFilter  = new ArrayList<SessionFilter>();
        SessionFactory factory = SessionFactory.getInstance();
        for(Session s: listSession){
            if(s instanceof SessionTitleIndex){
                listFilter.add(factory.createFilter(SessionFilter.TYPE_CONTENT, ((SessionTitleIndex) s).getDate()));
                LogUtils.logInfo("Date:"+((SessionTitleIndex) s).getDate());
            }
        }
        return listFilter;
    }

    private ArrayList<SessionFilter> createFilterByLocation(){
        ArrayList<SessionFilter> listFilter = new ArrayList<SessionFilter>();
        SessionFactory factory = SessionFactory.getInstance();
        int originSize = listSession.size();
        String pivot = "";
        for(int i=0;i<originSize;i++){
            Session s = listSession.get(i);
            if(!(s instanceof SessionTitleIndex)){
                if(!s.getPlace().equalsIgnoreCase(pivot)){
                    listFilter.add(factory.createFilter(SessionFilter.TYPE_CONTENT, s.getPlace()));
                    pivot = s.getPlace();
                    LogUtils.logInfo("Place:"+s.getPlace());
                }
            }
        }

        return listFilter;
    }



    public void showPopupSessionFilter() {
        final Button searchView = (((HomeActivity) mActivity).getSearchView());
        popupWidth = HomeActivity.searchViewWidth;
        searchViewHeight = HomeActivity.searchViewHeight;
        searchView.getLocationOnScreen(popupLocation);
        ViewGroup viewGroup = (LinearLayout) mActivity.findViewById(R.id.popup);
        LayoutInflater inflater = LayoutInflater.from(mActivity);
        layoutPopup = inflater.inflate(R.layout.menu_select_contact_type, viewGroup);
        popupWindow = new PopupWindow(mActivity);
        popupWindow.setContentView(layoutPopup);
        final ListView lv = (ListView) popupWindow.getContentView().findViewById(R.id.lv_contact_type);
        //final ArrayList<SessionFilter> listFilterComplete = createSessionFilerComplete();
        SessionFilterAdapter adapter = new SessionFilterAdapter(mActivity, listFilterAll);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //mActivity.makeToast(listFilterComplete.get(position).getTitle());
                SessionFilter sf = listFilterAll.get(position);
                if (sf.getType() == SessionFilter.TYPE_CONTENT) {
                    //searchSessionByFilter((SessionFilterContent) sf);
                    ((HomeActivity) mActivity).getSearchView().setText(((SessionFilterContent) sf).getTitle());
                    popupWindow.dismiss();
                    ((HomeActivity) mActivity).hideDarkBackground();
                    if(sf.getId() == SessionFilterContent.DEFAULT_ID_ALL){
                        loadAllSession();
                    }else if(sf.getId() == SessionFilterContent.DEFAULT_ID_BOOKMARKED){
                        getListSessionBookmarked();
                    }else if(sf.getId() == SessionFilterContent.DEFAULT_ID_CURRENT){
                        getCurrentSession();
                    }else {
                        searchSessionByCat((SessionFilterContent) sf);
                    }
                    //dialogSugguestSearch.dismiss();
                }
            }
        });
        popupWindow.setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.white)));
        popupWindow.setWidth(popupWidth + 60);
        //popupWindow.setHeight(popupHeight);
        popupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        popupWindow.setFocusable(true);

        LogUtils.logInfo("x=" + popupLocation[0] + ";y=" + popupLocation[1]);

        popupWindow.showAtLocation(layoutPopup, Gravity.NO_GRAVITY, popupLocation[0]-30, popupLocation[1] + searchViewHeight + 5);
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                ((HomeActivity)mActivity).hideDarkBackground();
            }
        });
        ((HomeActivity)mActivity).showDarkBackground();

    }

    private void getTemp(){
        LogUtils.logInfo("begin get temp, loc=:"+EchelonApplication.getInstance().getUser().getLocation());
        JsonHttpResponseHandler handler = new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    LogUtils.logInfo("finish get temp:"+response.toString());
                    boolean success = response.getBoolean(Constant.Key.KEY_SUCCESS);
                    if(success){
                        if(response.has(Constant.Key.KEY_CELCIUS)) {
                            int temp = (int) response.getDouble(Constant.Key.KEY_CELCIUS);
                            tvTemp.setText(Html.fromHtml(temp + MyTextUtils.toSup("o")));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                ConnectionUtils.logOut(getActivity());
            }
        };
        if(ConnectionUtils.checkConnection(mActivity)){
            if(EchelonApplication.getInstance().getEvent() == null) {
                ConnectionUtils.getTemperature(EchelonApplication.getInstance().getToken(), EchelonApplication.getInstance().getUser().getLocation(), handler);
            }else{
                ConnectionUtils.getTemperature(EchelonApplication.getInstance().getToken(), EchelonApplication.getInstance().getEvent().getLocation(), handler);
            }
        }
    }



    @Override
    public void onClickItemAdapter(int position) {
        Session s = adapter.getItem(position);
        if (!(s instanceof SessionTitleIndex)) {
            adapter.setFocusPosition(position);
            adapter.notifyDataSetChanged();
            Intent intent = new Intent(mActivity, SessionDetailActivity.class);
            intent.putExtra(SessionDetailActivity.KEY_SESSION_ID, s.getSession_id());
            intent.putExtra(Constant.Key.KEY_IS_BOOKMARK, s.is_bookmark());
            LogUtils.logInfo("session id = "+s.getSession_id());
            focusedPosition = position;
            startActivity(intent);
        }
    }

    public void getListSessionBookmarked(){
        JsonHttpResponseHandler handler = new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    mActivity.hideProgressDialog();
                    boolean success = response.getBoolean(Constant.Key.KEY_SUCCESS);
                    if(success){
                        JSONArray arrData = response.getJSONArray(Constant.Key.KEY_DATA);
                        listSessionBookmarked.clear();
                        Gson gson = new Gson();
                        for(int i=0;i<arrData.length();i++){
                            Session s = gson.fromJson(arrData.getJSONObject(i).toString(), Session.class);

                            listSessionBookmarked.add(s);
                        }
                        addSessionHeader(3);
                        adapter = new SessionAdapter(mActivity, listSessionBookmarked, AgendaFragment.this);
                        lv.setAdapter(adapter);
                        /*if(adapter.getCount() == 0){
                            layoutNoContent.setVisibility(View.VISIBLE);
                        }else{
                            layoutNoContent.setVisibility(View.GONE);
                        }*/
                        //
                        filterText = "Bookmarked";
                        ((HomeActivity)mActivity).getSearchView().setText(filterText);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                mActivity.hideProgressDialog();
                ConnectionUtils.logOut(getActivity());
            }
        };
        if(ConnectionUtils.checkConnection(mActivity)){
            mActivity.showProgressDialog("Loading...");
            ConnectionUtils.getBookmarkedSession(EchelonApplication.getInstance().getToken(), handler);
        }else{
            mActivity.makeToast(R.string.network_no);
        }
    }

    public void getCurrentSession(){
        JsonHttpResponseHandler handler = new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    mActivity.hideProgressDialog();
                    boolean success = response.getBoolean(Constant.Key.KEY_SUCCESS);
                    if(success){
                        JSONArray arrData = response.getJSONArray(Constant.Key.KEY_DATA);
                        listSessionCurrent.clear();
                        Gson gson = new Gson();
                        for(int i=0;i<arrData.length();i++){
                            Session s = gson.fromJson(arrData.getJSONObject(i).toString(), Session.class);
                            if (i == 0) {
                                listSessionCurrent.add(new SessionTitleIndex(s.getStart_at().substring(0, 10)));
                            }
                            listSessionCurrent.add(s);
                        }
                        adapter = new SessionAdapter(mActivity, listSessionCurrent, AgendaFragment.this);
                        lv.setAdapter(adapter);
                        /*if(adapter.getCount() == 0){
                            layoutNoContent.setVisibility(View.VISIBLE);
                        }else{
                            layoutNoContent.setVisibility(View.GONE);
                        }*/
                        //
                        filterText = "Current session";
                        ((HomeActivity)mActivity).getSearchView().setText(filterText);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                mActivity.hideProgressDialog();
                ConnectionUtils.logOut(getActivity());
            }
        };
        if(ConnectionUtils.checkConnection(mActivity)){

            mActivity.showProgressDialog("Loading...");
            ConnectionUtils.getCurrentSession(EchelonApplication.getInstance().getToken(), handler);
        }else{
            mActivity.makeToast(R.string.network_no);
        }

    }

    public void clearData(){
        listSession.clear();
        listSessionSearch.clear();
        listSessionCurrent.clear();
        listSessionBookmarked.clear();
        listFilterAll.clear();
    }

    public void notifiDatasetchanged(){
        if(lv!=null && lv.getAdapter()!=null) {
            if(updateAdapter){
                Session session = adapter.getItem(focusedPosition);
                boolean isBookmark = DataStore.getInstance().getBookmark(session.getSession_id());
                session.setIs_bookmark(isBookmark);
                adapter.notifyDataSetChanged();
                updateAdapter = false;
            }
        }
    }

    public String getFilterText() {
        return filterText;
    }

    public void setFilterText(String filterText) {
        this.filterText = filterText;
    }
}
