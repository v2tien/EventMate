package com.eventmate.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.eventmate.echelon.R;

/**
 * Created by ADMIN on 3/24/2016.
 */
public class WebViewTabFragment extends BaseFragment{

    private static final String KEY_URL = "URL";
    private String url = "";
    private ProgressBar loadingBar;
    public static WebView webView;
    private TextView tvNoContent;

    public static WebViewTabFragment newInstance(String url) {
        Bundle args = new Bundle();
        args.putString(KEY_URL, url);
        WebViewTabFragment fragment = new WebViewTabFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        url = getArguments().getString(KEY_URL);
        Log.e("url", url+"");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_session_qa, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    @Override
    protected void initView(View view) {
//        if (!url.equals("")) {
//            webView = (WebView) view.findViewById(R.id.wb_content);
//            webView.setWebViewClient(new MyBrowser());
//            webView.loadUrl(url);
//        }else {
//            tvNoContent = (TextView) view.findViewById(R.id.tv_no_content);
//            tvNoContent.setVisibility(View.VISIBLE);
//        }
        if (!url.equals("") || url != null) {
            loadingBar = (ProgressBar)view.findViewById(R.id.pb_load_web_progress);
            webView = (WebView) view.findViewById(R.id.wb_content);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setDomStorageEnabled(true);
            webView.getSettings().setUseWideViewPort(true);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.setWebViewClient(new MyBrowser());
            webView.setWebChromeClient(new WebChromeClient(){
                @Override
                public void onProgressChanged(WebView view, int progress) {
                    if (progress < 100
                            && loadingBar.getVisibility() == ProgressBar.GONE) {
                        loadingBar.setVisibility(ProgressBar.VISIBLE);
                    }
                    loadingBar.setProgress(progress);
                    if (progress == 100) {
                        loadingBar.setVisibility(ProgressBar.GONE);
                    }
                }
            });
            webView.loadUrl(url);
        }else {
            tvNoContent = (TextView) view.findViewById(R.id.tv_no_content);
            tvNoContent.setVisibility(View.VISIBLE);
        }
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

}
