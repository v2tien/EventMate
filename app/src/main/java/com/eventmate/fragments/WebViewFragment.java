package com.eventmate.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.eventmate.echelon.R;
import com.eventmate.utils.LogUtils;

/**
 * Created by ADMIN on 3/24/2016.
 */
public class WebViewFragment extends BaseFragment{

    private static final String KEY_URL = "URL";
    private String url = "";
    public static WebView wvContent;
    ProgressBar loadingBar;
    private TextView tvNoContent;

    public static WebViewFragment newInstance(String url) {
        LogUtils.logInfo("Url="+url);
        Bundle args = new Bundle();
        args.putString(KEY_URL, url);
        WebViewFragment fragment = new WebViewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        url = getArguments().getString(KEY_URL);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_session_qa, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    @Override
    protected void initView(View view) {
        if (!url.equals("") || url != null) {
            loadingBar = (ProgressBar)view.findViewById(R.id.pb_load_web_progress);
            wvContent = (WebView) view.findViewById(R.id.wb_content);
            wvContent.getSettings().setJavaScriptEnabled(true);
            wvContent.getSettings().setDomStorageEnabled(true);
            wvContent.getSettings().setUseWideViewPort(true);
            wvContent.getSettings().setLoadWithOverviewMode(true);
            wvContent.setWebViewClient(new MyBrowser());
            wvContent.setWebChromeClient(new WebChromeClient(){
                @Override
                public void onProgressChanged(WebView view, int progress) {
                    if (progress < 100
                            && loadingBar.getVisibility() == ProgressBar.GONE) {
                        loadingBar.setVisibility(ProgressBar.VISIBLE);
                    }
                    loadingBar.setProgress(progress);
                    if (progress == 100) {
                        loadingBar.setVisibility(ProgressBar.GONE);
                    }
                }
            });
            wvContent.loadUrl(url);
        }else {
            tvNoContent = (TextView) view.findViewById(R.id.tv_no_content);
            tvNoContent.setVisibility(View.VISIBLE);
        }
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

}
