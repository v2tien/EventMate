package com.eventmate.models;

import com.google.gson.Gson;

import java.io.Serializable;

/**
 * Created by Trang on 3/24/2016.
 */
public class User implements Serializable {

    private String contact_id,first_name, last_name, code, img, cell_phone;
    private String email, age, job_title,blog, company, birth_date, gender, img_background;
    private String url_facebook,url_linkedin,url_twitter;
    private String information_url,map_url, website, location;
    private int type;
    private boolean is_bookmark;

    public User(){}

    public User(String contact_id, String first_name, String last_name, String img) {
        this.contact_id = contact_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.img = img;
    }

    public String toJSon(){
        return new Gson().toJson(this);
    }

    public String getUrl_facebook() {
        return url_facebook;
    }

    public void setUrl_facebook(String url_facebook) {
        this.url_facebook = url_facebook;
    }

    public String getUrl_linkedin() {
        return url_linkedin;
    }

    public void setUrl_linkedin(String url_linkedin) {
        this.url_linkedin = url_linkedin;
    }

    public String getUrl_twitter() {
        return url_twitter;
    }

    public void setUrl_twitter(String url_twitter) {
        this.url_twitter = url_twitter;
    }

    public String getInformation_url() {
        return information_url;
    }

    public void setInformation_url(String information_url) {
        this.information_url = information_url;
    }

    public String getMap_url() {
        return map_url;
    }

    public void setMap_url(String map_url) {
        this.map_url = map_url;
    }

    public boolean is_bookmark() {
        return is_bookmark;
    }

    public void setIs_bookmark(boolean is_bookmark) {
        this.is_bookmark = is_bookmark;
    }

    public String getBlog() {
        return blog;
    }

    public void setBlog(String blog) {
        this.blog = blog;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getContact_id() {
        return contact_id;
    }

    public void setContact_id(String contact_id) {
        this.contact_id = contact_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getCell_phone() {
        return cell_phone;
    }

    public void setCell_phone(String cell_phone) {
        this.cell_phone = cell_phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getJob_title() {
        if(job_title == null){
            return "";
        }
        return job_title;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int compare(User other){
        String fullName = first_name+" "+last_name;
        String otherFullName = other.getFirst_name()+" "+other.getLast_name();
        return fullName.compareTo(otherFullName);
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getImg_background() {
        return img_background;
    }

    public void setImg_background(String img_background) {
        this.img_background = img_background;
    }
}
