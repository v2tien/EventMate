package com.eventmate.models;

/**
 * Created by ADMIN on 3/26/2016.
 */
public class SessionFactory {
    private static SessionFactory INSTANCE;

    public static SessionFactory getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new SessionFactory();
        }
        return INSTANCE;
    }

    public SessionFilter createFilter(int type, String title) {
        if (type == SessionFilter.TYPE_HEADER) {
            return new SessionFilterHeader(title);
        } else if (type == SessionFilter.TYPE_CONTENT) {
            return new SessionFilterContent(title);
        } else {
            return null;
        }
    }
}
