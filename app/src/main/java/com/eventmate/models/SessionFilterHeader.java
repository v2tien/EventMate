package com.eventmate.models;

/**
 * Created by ADMIN on 3/26/2016.
 */
public class SessionFilterHeader extends SessionFilter {

    public static final int DEFAULT_ID = -88;

    public SessionFilterHeader(String title) {
        this.title = title;
    }

    public SessionFilterHeader(int id,String title) {
        this.id = id;
        this.title = title;
    }

    @Override
    public int getType() {
        return SessionFilter.TYPE_HEADER;
    }
}
