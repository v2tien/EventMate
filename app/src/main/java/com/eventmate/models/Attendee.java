package com.eventmate.models;

/**
 * Created by ADMIN on 3/25/2016.
 */
public class Attendee {
    String name;
    String description;

    public Attendee(String name, String description){
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

