package com.eventmate.models;

/**
 * Created by ADMIN on 3/26/2016.
 */
public abstract class SessionFilter {

    public static int TYPE_HEADER = 69;
    public static int TYPE_CONTENT = 96;
    String title;
    int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public abstract int getType();

}
