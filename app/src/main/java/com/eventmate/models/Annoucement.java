package com.eventmate.models;

public class Annoucement {
    String name;
    String description;
    String by;
    String under;
    String url;
    String title;

    public Annoucement(String name, String description, String by, String under){
        this.name = name;
        this.description = description;
        this.by = by;
        this.under = under;
    }

    public Annoucement(String name) {
        this.name = name;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        if(description!=null) {
            return description;
        }else {
            return "Not have data yet";
        }
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBy() {
        if(by!=null) {
            return by;
        }else{
            return "Not have data yet";
        }
    }

    public void setBy(String by) {
        this.by = by;
    }

    public String getUnder() {
        if(under!=null) {
            return under;
        }else {
            return "Not have data yet";
        }
    }

    public void setUnder(String under) {
        this.under = under;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
