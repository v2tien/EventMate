package com.eventmate.models;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by ADMIN on 3/26/2016.
 */
public class SessionFilterContent extends SessionFilter {

    public static final int DEFAULT_ID_ALL = -1;
    public static final int DEFAULT_ID_BOOKMARKED = -2;
    public static final int DEFAULT_ID_CURRENT = -3;


    public SessionFilterContent(String title) {
        this.title = title;
    }

    public SessionFilterContent(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public boolean isDateType() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            df.parse(title);
            return true;
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }



    @Override
    public int getType() {
        return SessionFilter.TYPE_CONTENT;
    }
}
