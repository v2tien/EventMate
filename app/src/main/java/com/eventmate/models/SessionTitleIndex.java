package com.eventmate.models;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class SessionTitleIndex extends Session {

    String date;
    String info = "";

    public SessionTitleIndex(String date) {
        Calendar c = Calendar.getInstance();
        this.date = date;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date thisDate = df.parse(date);
            c.setTime(thisDate);
            int dayNumber = c.get(Calendar.DAY_OF_WEEK);
            int monthNumber = c.get(Calendar.MONTH);
            switch (dayNumber){
                case 1:
                    info = info+"Sunday, ";
                    break;
                case 2:
                    info = info+"Monday, ";
                    break;
                case 3:
                    info = info+"Tuesday, ";
                    break;
                case 4:
                    info = info+"Wednesday, ";
                    break;
                case 5:
                    info = info+"Thursday, ";
                    break;
                case 6:
                    info = info+"Friday, ";
                    break;
                case 7:
                    info = info+"Saturday, ";
                    break;
            }
            switch (monthNumber){
                case 0:
                    info= info+"Jan";
                    break;
                case 1:
                    info= info+"Feb";
                    break;
                case 2:
                    info= info+"Mar";
                    break;
                case 3:
                    info= info+"Apr";
                    break;
                case 4:
                    info= info+"May";
                    break;
                case 5:
                    info= info+"Jun";
                    break;
                case 6:
                    info= info+"Jul";
                    break;
                case 7:
                    info= info+"Aug";
                    break;
                case 8:
                    info= info+"Sep";
                    break;
                case 9:
                    info= info+"Oct";
                    break;
                case 10:
                    info= info+"Nov";
                    break;
                case 11:
                    info= info+"Dec";
                    break;

            }
            info = info+" "+Integer.toString(c.get(Calendar.DAY_OF_MONTH));
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getInfo() {
        if(info!=null&& !info.isEmpty()) {
            return info;
        }else{
            return getPlace();
        }
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
