package com.eventmate.models;

import com.eventmate.echelon.R;

public class LeftMenuItem {

    public static final int TYPE_NORMAL = 0;

    private int icon = 0x0;
    private String title;
    private int iconFocused;
    private int type = 1; // 0 is normal. 1 is custom
    // using for custom menu item
    private String icon_url, icon_hover_url, page_url;


    public LeftMenuItem(int icon, String title, int type){
        this.type = type;
        this.icon = icon;
        this.title = title;
        getIconFocused();
    }

    public int getIconFocused(){
        switch (icon){
            case R.drawable.ic_speaker:
                iconFocused =R.drawable.ic_speaker_click;
                break;
            case R.drawable.ic_agenda:
                iconFocused =R.drawable.ic_agenda_click;
                break;
            case R.drawable.ic_bag:
                iconFocused =R.drawable.ic_bag_click;
                break;
            case R.drawable.ic_map:
                iconFocused =R.drawable.ic_map_click;
                break;
            case R.drawable.ic_micro:
                iconFocused =R.drawable.ic_micro_click;
                break;
            case R.drawable.ic_attende:
                iconFocused =R.drawable.ic_attende_click;
                break;
            case R.drawable.ic_money:
                iconFocused =R.drawable.ic_money_click;
                break;
            case R.drawable.ic_glass:
                iconFocused =R.drawable.ic_glass_click;
                break;
        }
        return iconFocused;
    }

    public int getIcon() {
        switch (iconFocused){
            case R.drawable.ic_speaker_click:
                iconFocused =R.drawable.ic_speaker;
                break;
            case R.drawable.ic_agenda_click:
                iconFocused =R.drawable.ic_agenda;
                break;
            case R.drawable.ic_bag_click:
                iconFocused =R.drawable.ic_bag;
                break;
            case R.drawable.ic_map_click:
                iconFocused =R.drawable.ic_map;
                break;
            case R.drawable.ic_micro_click:
                iconFocused =R.drawable.ic_micro;
                break;
            case R.drawable.ic_attende_click:
                iconFocused =R.drawable.ic_attende;
                break;
            case R.drawable.ic_money_click:
                iconFocused =R.drawable.ic_money;
                break;
            case R.drawable.ic_glass_click:
                iconFocused =R.drawable.ic_glass;
                break;
        }
        return icon;
    }

    public int getType() {
        return type;
    }

    public String getIcon_url() {
        return icon_url;
    }

    public void setIcon_url(String icon_url) {
        this.icon_url = icon_url;
    }

    public String getIcon_hover_url() {
        return icon_hover_url;
    }

    public void setIcon_hover_url(String icon_hover_url) {
        this.icon_hover_url = icon_hover_url;
    }

    public String getPage_url() {
        return page_url;
    }

    public void setPage_url(String page_url) {
        this.page_url = page_url;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
