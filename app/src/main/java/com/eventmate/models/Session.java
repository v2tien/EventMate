package com.eventmate.models;

import com.eventmate.echelon.EchelonApplication;
import com.eventmate.utils.LogUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Trang on 3/25/2016.
 */
public class Session {


    private String session_id;
    private String event_id;
    private String title, start_at, end_at, place, description, map;
    private String created_at, last_update;
    private List<User> categories;
    private List<User> speakers;
    private List<User> moderators;
    private List<User> attendees;
    private String m;
    private int total_attendee, total_rate;
    private boolean is_bookmark;


    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getTitle() {
        return title==null?"null":title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStart_at() {
        return start_at;
    }

    public void setStart_at(String start_at) {
        this.start_at = start_at;
    }

    public String getEnd_at() {
        return end_at;
    }

    public void setEnd_at(String end_at) {
        this.end_at = end_at;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDescription() {
        if(description!=null) {
            return description;
        }else{
            return "Description";
        }
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }

    public List<User> getAttendees() {
        if (attendees == null)
            attendees = new ArrayList<>();
        return attendees;
    }

    public void setAttendees(List<User> attendees) {
        this.attendees = attendees;
    }

    public List<User> getModerators() {
        if (moderators == null)
            moderators = new ArrayList<>();
        return moderators;
    }

    public void setModerators(List<User> moderators) {
        this.moderators = moderators;
    }

    public List<User> getCategories() {
        if (categories == null)
            categories = new ArrayList<>();
        return categories;
    }

    public void setCategories(List<User> categories) {
        this.categories = categories;
    }

    public List<User> getSpeakers() {
        if (speakers == null)
            speakers = new ArrayList<>();
        return speakers;
    }

    public void setSpeakers(List<User> speakers) {
        this.speakers = speakers;
    }

    public int compare(Session session) {
        LogUtils.logInfo(start_at.substring(0,10));
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date thisDate = null;
        Date otherDate = null;
        try {
            thisDate = df.parse(start_at.substring(0, 10));
            otherDate = df.parse(session.getStart_at().substring(0, 10));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (thisDate.before(otherDate)) {
            return -1;
        } else if (thisDate.after(otherDate)) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean isEqualDate(Session session){
        String thisDate = start_at.substring(0,10);
        String otherDate = session.getStart_at().substring(0,10);
        return thisDate.equals(otherDate);
    }

    public String getM(){
        int hours = Integer.parseInt(start_at.substring(11, 13));
        if(hours<12){
            return " am";
        }else {
            return " pm";
        }
    }

    public int getTotal_attendee() {
        return total_attendee;
    }

    public void setTotal_attendee(int total_attendee) {
        this.total_attendee = total_attendee;
    }

    public int getTotal_rate() {
        return total_rate;
    }

    public void setTotal_rate(int total_rate) {
        this.total_rate = total_rate;
    }

    public boolean is_bookmark() {
        return is_bookmark;
    }

    public void setIs_bookmark(boolean is_bookmark) {
        this.is_bookmark = is_bookmark;
    }

    @Override
    public String toString() {
        return EchelonApplication.gson.toJson(this).toString();
    }
}
