package com.eventmate.models;

public class ContactType {

    int contact_type_id;
    int user_id;
    String title;
    String created_at;
    String last_update;

    public static final int DEFAULT_ID = -69;
    public static final int DEFAULT_ID_BOOKMARKED = -96;

    public ContactType() {

    }

    public ContactType(int contact_type_id, String title){
        this.title = title;
        this.contact_type_id = contact_type_id;
    }

    public int getContact_type_id() {
        return contact_type_id;
    }

    public void setContact_type_id(int contact_type_id) {
        this.contact_type_id = contact_type_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }
}
