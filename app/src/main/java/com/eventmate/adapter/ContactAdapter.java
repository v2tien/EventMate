package com.eventmate.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.eventmate.echelon.BaseActivity;
import com.eventmate.echelon.R;
import com.eventmate.fragments.AttendeesFragment;
import com.eventmate.models.User;
import com.eventmate.utils.LogUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ADMIN on 3/26/2016.
 */
public class ContactAdapter extends BaseAdapter{

    BaseActivity context;
    ArrayList<User> listContact;
    LayoutInflater inflater;
    IContactAdapterListenner contactAdapterListenner;

    public interface IContactAdapterListenner{
        void onClickContactItem(int position);
        void onClickBookMark(int position);
    }

    public ContactAdapter( BaseActivity context,  ArrayList<User> listContact, IContactAdapterListenner contactAdapterListenner){
        this.context = context;
        this.listContact = listContact;
        inflater = LayoutInflater.from(context);
        this.contactAdapterListenner = contactAdapterListenner;

    }
    @Override
    public int getCount() {
        return listContact.size();
    }

    @Override
    public User getItem(int position) {
        return listContact.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        User contact = getItem(position);
        if(convertView == null){
            convertView = inflater.inflate(R.layout.item_attendee, parent, false);
            holder = new ViewHolder();
            holder.tvName = (TextView)convertView.findViewById(R.id.tv_name);
            holder.tvDescription = (TextView)convertView.findViewById(R.id.tv_description);
            holder.tvAvatar = (TextView)convertView.findViewById(R.id.tv_avatar);
            holder.ivAvatar = (CircleImageView)convertView.findViewById(R.id.iv_avatar);
            holder.btnBookmark = (ImageView) convertView.findViewById(R.id.btn_bookmark);
            holder.tvContactHeader = (TextView)convertView.findViewById(R.id.tv_attendee_header);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder)convertView.getTag();
        }
        if(position == 0){
            holder.tvContactHeader.setVisibility(View.VISIBLE);
            if(contact.getFirst_name()!=null && !contact.getFirst_name().isEmpty()) {
                holder.tvContactHeader.setText(Character.toString(contact.getFirst_name().charAt(0)).toUpperCase());
            }
        }else if(position>0){
            String prevContact = "";
            String currentContact = "";
            if(contact.getFirst_name()!=null && !contact.getFirst_name().isEmpty()&& getItem(position-1).getFirst_name()!=null && !getItem(position-1).getFirst_name().isEmpty()){
                currentContact = Character.toString(contact.getFirst_name().charAt(0)).toUpperCase();
                prevContact = Character.toString(getItem(position-1).getFirst_name().charAt(0)).toUpperCase();
            }
            if(currentContact.equals(prevContact)){
                holder.tvContactHeader.setVisibility(View.GONE);
            }else{
                holder.tvContactHeader.setVisibility(View.VISIBLE);
                holder.tvContactHeader.setText(Character.toString(contact.getFirst_name().charAt(0)).toUpperCase());
            }
        }
        if(contact.getImg()==null||contact.getImg().isEmpty()){
            holder.tvAvatar.setVisibility(View.VISIBLE);
            if(contact.getFirst_name()!=null && !contact.getFirst_name().isEmpty()&& contact.getLast_name()!=null&& !contact.getLast_name().isEmpty()) {
                holder.tvAvatar.setText(Character.toString(contact.getFirst_name().charAt(0)).toUpperCase() + Character.toString(contact.getLast_name().charAt(0)).toUpperCase());
            }
            holder.ivAvatar.setVisibility(View.GONE);
        }else{
            holder.ivAvatar.setVisibility(View.VISIBLE);
            Picasso.with(context).load(contact.getImg()).into(holder.ivAvatar);
            holder.tvAvatar.setVisibility(View.GONE);
        }

        if (contact.is_bookmark()){
            holder.btnBookmark.setImageResource(R.drawable.ic_bookmark_selected);
            //holder.btnBookmark.setEnabled(false);
        }else{
            holder.btnBookmark.setImageResource(R.drawable.ic_bookmark_unselected);
            //holder.btnBookmark.setEnabled(true);
        }
        holder.tvName.setText(contact.getFirst_name() + " " + contact.getLast_name());
        holder.btnBookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.logInfo("pos=" + position);
                AttendeesFragment.getInstance().onClickBookMark(position);
            }
        });
        if(!contact.getJob_title().isEmpty()) {
            holder.tvDescription.setVisibility(View.VISIBLE);
            holder.tvDescription.setText(contact.getJob_title());
        }else{
            holder.tvDescription.setVisibility(View.GONE);
        }
        /*// event click
        holder.btnBookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactAdapterListenner.onClickBookMark(position);
            }
        });
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactAdapterListenner.onClickContactItem(position);
            }
        });*/

        //holder.tvDescription.setText(contact.getBio());
        return convertView;
    }

    public ArrayList<User> getListContact() {
        return listContact;
    }

    public void setListContact(ArrayList<User> listContact) {
        this.listContact = listContact;
    }

    private static class ViewHolder{
        TextView tvName, tvDescription, tvAvatar;
        CircleImageView ivAvatar;
        ImageView btnBookmark;
        TextView tvContactHeader;
    }
}
