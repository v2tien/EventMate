package com.eventmate.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.eventmate.echelon.R;
import com.eventmate.models.User;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Trang on 3/24/2016.
 */
public class AttendingAdapter  extends RecyclerView.Adapter<AttendingAdapter.ViewHolder> {

    Context context;
    List<User> users;
    IListennerAttendingAdapter listennerSpeakerAdapter;

    public interface IListennerAttendingAdapter{
        void onClickAttending(int position);

    }

    public AttendingAdapter(Context context, List<User> users, IListennerAttendingAdapter listennerSpeakerAdapter) {
        this.context = context;
        this.users = users;
        this.listennerSpeakerAdapter = listennerSpeakerAdapter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_session_attending,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        User user = users.get(position);
        if (user != null){
            if (user.getImg() != null) {
                Picasso.with(context).load(user.getImg()).error(R.drawable.ic_event_logo_small).into(holder.imgAvartar);
                holder.imgAvartar.setVisibility(View.VISIBLE);
                holder.tvAvartar.setVisibility(View.GONE);
            }else{
                holder.imgAvartar.setVisibility(View.GONE);
                holder.tvAvartar.setVisibility(View.VISIBLE);
                holder.tvAvartar.setText(Character.toString(user.getFirst_name().charAt(0)).toUpperCase() + Character.toString(user.getLast_name().charAt(0)).toUpperCase());
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listennerSpeakerAdapter.onClickAttending(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imgAvartar;
        TextView tvAvartar;

        public ViewHolder(View itemView) {
            super(itemView);
            imgAvartar = (ImageView) itemView.findViewById(R.id.img_avartar);
            tvAvartar = (TextView) itemView.findViewById(R.id.tv_avatar);
        }
    }
}
