package com.eventmate.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.eventmate.echelon.R;
import com.eventmate.models.LeftMenuItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MenuLeftAdapter extends BaseAdapter{

    Context context;
    ArrayList<LeftMenuItem> listMenuItems;
    LayoutInflater inflater;
    int selectedPos = 0;

    public MenuLeftAdapter(Context context, ArrayList<LeftMenuItem> listMenuItems){
        this.context = context;
        this.listMenuItems = listMenuItems;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listMenuItems.size();
    }

    @Override
    public LeftMenuItem getItem(int position) {
        return listMenuItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LeftMenuItem leftMenuItem = getItem(position);
        if(convertView == null){
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_left_menu, parent, false);
            holder.ivIcon = (ImageView)convertView.findViewById(R.id.iv_menu_item_icon);
            holder.tvTitle = (TextView)convertView.findViewById(R.id.tv_menu_item_title);
            holder.seperate = convertView.findViewById(R.id.view_seperate);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder)convertView.getTag();
        }
        if(leftMenuItem.getIcon()!=0x0) {
            holder.ivIcon.setImageResource(leftMenuItem.getIcon());
        }else{
            Picasso.with(context).load(leftMenuItem.getIcon_url()).into(holder.ivIcon);
        }
        holder.tvTitle.setText(leftMenuItem.getTitle());
        if(position == 3|| position == 6){
            //holder.seperate.setVisibility(View.VISIBLE);
            holder.seperate.setVisibility(View.GONE);
        }else {
            holder.seperate.setVisibility(View.GONE);
        }
        if(position == selectedPos){
            convertView.setBackgroundColor(context.getResources().getColor(R.color.color_selected_background_left_menu));
            holder.tvTitle.setTextColor(context.getResources().getColor(R.color.color_text_color_left_menu_focus));
            if(leftMenuItem.getIcon()!=0x0) {
                holder.ivIcon.setImageResource(leftMenuItem.getIconFocused());
            }else{
                Picasso.with(context).load(leftMenuItem.getIcon_hover_url()).into(holder.ivIcon);
            }
        }else{
            convertView.setBackgroundColor(context.getResources().getColor(android.R.color.white));
            holder.tvTitle.setTextColor(context.getResources().getColor(R.color.color_text_color_left_menu));
            if(leftMenuItem.getIcon()!=0x0) {
                holder.ivIcon.setImageResource(leftMenuItem.getIcon());
            }else{
                Picasso.with(context).load(leftMenuItem.getIcon_url()).into(holder.ivIcon);
            }
        }
        return convertView;
    }

    public int getSelectedPos() {
        return selectedPos;
    }

    public void setSelectedPos(int selectedPos) {
        this.selectedPos = selectedPos;
    }

    private static class ViewHolder{
        ImageView ivIcon;
        TextView tvTitle;
        View seperate;
    }
}
