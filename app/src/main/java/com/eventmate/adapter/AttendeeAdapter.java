package com.eventmate.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.eventmate.echelon.BaseActivity;
import com.eventmate.echelon.R;
import com.eventmate.models.Attendee;

import java.util.ArrayList;

public class AttendeeAdapter extends BaseAdapter{

    BaseActivity context;
    ArrayList<Attendee> listAttendees;
    LayoutInflater inflater;

    public AttendeeAdapter(BaseActivity context, ArrayList<Attendee> listAttendees){
        this.context = context;
        this.listAttendees= listAttendees;
        inflater= LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listAttendees.size();
    }

    @Override
    public Attendee getItem(int position) {
        return listAttendees.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        Attendee attendee = getItem(position);
        if(convertView == null){
            convertView = inflater.inflate(R.layout.item_attendee, parent, false);
            holder = new ViewHolder();
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder)convertView.getTag();
        }

        return convertView;
    }

    private static class ViewHolder{

    }
}
