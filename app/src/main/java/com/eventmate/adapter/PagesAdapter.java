package com.eventmate.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.azstack.fragment.AzConversationFragment;
import com.eventmate.echelon.EchelonApplication;
import com.eventmate.fragments.AgendaFragment;
import com.eventmate.fragments.AttendeesFragment;
import com.eventmate.fragments.WebViewFragment;
import com.eventmate.fragments.WebViewTabFragment;

public class PagesAdapter extends FragmentPagerAdapter {

    public PagesAdapter(FragmentManager fm) {
        super(fm);
    }

    // TODO: 3/28/2016 Need remove google.com
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
//                return WebViewFragment.newInstance("http://www.google.com");
                return new AzConversationFragment();
            case 3:
                return WebViewTabFragment.newInstance(EchelonApplication.getInstance().getUser().getMap_url());
            case 4:
                return WebViewFragment.newInstance( EchelonApplication.getInstance().getUser().getInformation_url());
            case 2:
                return AgendaFragment.getInstance();
            case 1:
                return AttendeesFragment.getInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 5;
    }
}
