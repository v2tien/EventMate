package com.eventmate.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eventmate.echelon.R;
import com.eventmate.impl.IAdapterListenner;
import com.eventmate.models.Event;

import java.util.List;

/**
 * Created by Trang on 3/26/2016.
 */
public class EventChooseAdapter  extends RecyclerView.Adapter<EventChooseAdapter.ViewHolder>{

    List<Event> listEvent;
    Context context;
    IAdapterListenner adapterListenner;

    public EventChooseAdapter(List<Event> listEvent, Context context, IAdapterListenner adapterListenner) {
        this.listEvent = listEvent;
        this.context = context;
        this.adapterListenner = adapterListenner;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Event event = listEvent.get(position);
        if (event != null){
            holder.tvTitle.setText(event.getTitle());
            String date = "\t"+event.getCreated_at().substring(0,10).toString();
            holder.tvDate.setText(date);
            holder.tvLocation.setText("\t"+event.getLocation());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    adapterListenner.onClickItemAdapter(position);
                }
            });
            if (position == getItemCount()-1){
                holder.line.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return listEvent.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView tvTitle, tvDate, tvLocation;
        View line;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvDate = (TextView) itemView.findViewById(R.id.tv_date);
            tvLocation = (TextView) itemView.findViewById(R.id.tv_location);
            line = itemView.findViewById(R.id.line);
        }
    }
}
