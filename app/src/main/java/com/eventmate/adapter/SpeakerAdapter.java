package com.eventmate.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.eventmate.echelon.R;
import com.eventmate.models.User;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Trang on 3/24/2016.
 */
public class SpeakerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_VIEW_NORMAL = 1;
    private static final int TYPE_VIEW_END = 0;

    Context context;
    List<User> users;
    IListennerSpeakerAdapter listennerSpeakerAdapter;

    public interface IListennerSpeakerAdapter{
        void onClickSpeaker(int position);
    }

    public SpeakerAdapter(Context context, List<User> users, IListennerSpeakerAdapter listennerSpeakerAdapter) {
        this.context = context;
        this.users = users;
        this.listennerSpeakerAdapter = listennerSpeakerAdapter;
    }

    @Override
    public int getItemViewType(int position) {
        if (users.get(position) != null)
            return TYPE_VIEW_NORMAL;
        return TYPE_VIEW_END;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == TYPE_VIEW_NORMAL) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_session_speaker, parent, false);
            return new ViewNormal(view);
        }else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_session_speaker_end, parent, false);
            return new ViewEnd(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof ViewNormal) {
            ViewNormal viewHolder = (ViewNormal) holder;
            User user = users.get(position);
            if (user != null) {
                viewHolder.tvName.setText(user.getFirst_name() +" "+ user.getLast_name());

                if (user.getImg() != null) {
                    Picasso.with(context).load(user.getImg()).error(R.drawable.ic_event_logo).into(viewHolder.imgAvartar);
                    viewHolder.imgAvartar.setVisibility(View.VISIBLE);
                    viewHolder.tvAvartar.setVisibility(View.GONE);
                }else{
                    viewHolder.imgAvartar.setVisibility(View.GONE);
                    viewHolder.tvAvartar.setVisibility(View.VISIBLE);
                    viewHolder.tvAvartar.setText(Character.toString(user.getFirst_name().charAt(0)).toUpperCase() + Character.toString(user.getLast_name().charAt(0)).toUpperCase());
                }

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listennerSpeakerAdapter.onClickSpeaker(position);
                    }
                });


            }
        }
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewNormal extends RecyclerView.ViewHolder{
        ImageView imgAvartar;
        TextView tvName;
        TextView tvAvartar;

        public ViewNormal(View itemView) {
            super(itemView);
            imgAvartar = (ImageView) itemView.findViewById(R.id.img_avartar);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvAvartar = (TextView) itemView.findViewById(R.id.tv_avatar);
        }
    }

    public class ViewEnd extends RecyclerView.ViewHolder{
        View img;

        public ViewEnd(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.divider);
        }
    }
}
