package com.eventmate.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.eventmate.echelon.BaseActivity;
import com.eventmate.echelon.R;
import com.eventmate.models.ContactType;

import java.util.ArrayList;

/**
 * Created by ADMIN on 3/28/2016.
 */
public class ContactTypeAdapter extends BaseAdapter{

    BaseActivity context;
    ArrayList<ContactType> listContactType;
    LayoutInflater inflater;

    public ContactTypeAdapter(BaseActivity context, ArrayList<ContactType> listContactType){
        this.context = context;
        this.listContactType = listContactType;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listContactType.size();
    }

    @Override
    public ContactType getItem(int position) {
        return listContactType.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        ContactType contactType = getItem(position);
        if(convertView == null){
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_menu_search_contact, parent, false);
            holder.tvTitle = (TextView)convertView.findViewById(R.id.tv_menu_item_title);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder)convertView.getTag();
        }
        holder.tvTitle.setText(contactType.getTitle());
        return convertView;
    }

    private static class ViewHolder{
        TextView tvTitle;
    }
}
