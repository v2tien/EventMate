package com.eventmate.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eventmate.echelon.R;
import com.eventmate.impl.IAdapterListenner;
import com.eventmate.models.Annoucement;
import com.vipul.hp_hp.timelineview.TimelineView;

import java.util.ArrayList;

public class AnnoucementAdapter extends RecyclerView.Adapter<AnnoucementAdapter.ViewHolder> {

    ArrayList<Annoucement> listAnnoucements;
    IAdapterListenner adapterListenner;

    public AnnoucementAdapter(ArrayList<Annoucement> listAnnoucements, IAdapterListenner iAdapterListenner) {
        this.listAnnoucements = listAnnoucements;
        this.adapterListenner = iAdapterListenner;
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position, listAnnoucements.size());
    }

    @Override
    public AnnoucementAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.item_annoucement, null);
        return new ViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(final AnnoucementAdapter.ViewHolder holder, final int position) {
        Annoucement annoucement = listAnnoucements.get(position);
        holder.tvAnnoucementName.setText(annoucement.getTitle());
        holder.tvAnnoucementDescription.setText(annoucement.getDescription());
        holder.tvAnnoucementBy.setText(annoucement.getBy());
        holder.tvAnnoucementUnder.setText(annoucement.getUnder());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapterListenner.onClickItemAdapter(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listAnnoucements.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvAnnoucementName,tvAnnoucementDescription, tvAnnoucementBy, tvAnnoucementUnder;
        TimelineView timelineView;

        public ViewHolder(View itemView, int viewType) {
            super(itemView);
            tvAnnoucementName = (TextView) itemView.findViewById(R.id.tv_annoucement_name);
            tvAnnoucementDescription=(TextView) itemView.findViewById(R.id.tv_annoucement_description);
            tvAnnoucementBy=(TextView) itemView.findViewById(R.id.tv_annoucement_by);
            tvAnnoucementUnder=(TextView) itemView.findViewById(R.id.tv_annoucement_under);
            timelineView = (TimelineView) itemView.findViewById(R.id.timeline_view);
            timelineView.initLine(viewType);
        }
    }
}
