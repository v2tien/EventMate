package com.eventmate.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.eventmate.echelon.BaseActivity;
import com.eventmate.echelon.R;
import com.eventmate.models.ContactType;
import com.eventmate.models.SessionFilter;
import com.eventmate.models.SessionFilterContent;
import com.eventmate.utils.LogUtils;

import java.util.ArrayList;

public class SessionFilterAdapter extends BaseAdapter {

    BaseActivity context;
    ArrayList<SessionFilter> listFilter;
    LayoutInflater inflater;

    public SessionFilterAdapter(BaseActivity context, ArrayList<SessionFilter> listFilter) {
        this.context = context;
        this.listFilter = listFilter;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listFilter.size();
    }

    @Override
    public SessionFilter getItem(int position) {
        return listFilter.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /*@Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        SessionFilter sf = getItem(position);
        if (convertView == null) {
            *//*if (sf.getType() == SessionFilter.TYPE_HEADER) {
                convertView = inflater.inflate(R.layout.item_session_filter_header, parent, false);
                holder = new ViewHolder();
                holder.tvHeader = (TextView) convertView.findViewById(R.id.tv_header);
            } else if(sf.getType() == SessionFilter.TYPE_CONTENT){
                convertView = inflater.inflate(R.layout.item_session_filter_content, parent, false);
                holder = new ViewHolder();
                holder.tvContent = (TextView) convertView.findViewById(R.id.tv_content_1);
                LogUtils.logInfo("textview null:" + (holder.tvContent == null));
            }*//*
            convertView = inflater.inflate(R.layout.item_session_filter_header, parent, false);
            holder = new ViewHolder();
            holder.tvHeader = (TextView) convertView.findViewById(R.id.tv_header);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        *//*if (sf.getType() == SessionFilter.TYPE_HEADER) {
            LogUtils.logInfo(sf.getType()+"");
            holder.tvHeader.setText(sf.getTitle());
        } else {
            LogUtils.logInfo(sf.getType()+"");
            holder.tvContent.setText(sf.getTitle());
        }*//*
        if (sf.getType() == SessionFilter.TYPE_HEADER) {
            holder.tvHeader.setText(sf.getTitle());
        } else {
            holder.tvHeader.setPadding(35, 0, 0, 0);
            holder.tvHeader.setTextSize(17);
            holder.tvHeader.setTypeface(null, Typeface.NORMAL);
            holder.tvHeader.setText(sf.getTitle());
        }
        return convertView;
    }*/
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        SessionFilter sf = getItem(position);
        if (convertView == null) {
            if (sf.getType() == SessionFilter.TYPE_HEADER) {
                LogUtils.logInfo("header");
                convertView = inflater.inflate(R.layout.item_session_filter_header, parent, false);
                holder = new ViewHolder();
                holder.tvHeader = (TextView) convertView.findViewById(R.id.tv_header);
            } else{
                LogUtils.logInfo("content");
                convertView = inflater.inflate(R.layout.item_session_filter_content, parent, false);
                holder = new ViewHolder();
                holder.tvContent = (TextView) convertView.findViewById(R.id.tv_content_1);
            }
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (sf.getType() == SessionFilter.TYPE_HEADER) {
            if(holder.tvHeader!=null) {
                holder.tvHeader.setText(sf.getTitle());
            }
        } else {
            if(holder.tvContent!=null) {
                holder.tvContent.setText(sf.getTitle());
            }
        }

        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        SessionFilter sf = getItem(position);
        if(sf instanceof SessionFilterContent){
            return 0;
        }else{
            return 1;
        }
    }

    private static class ViewHolder {
        TextView tvHeader;
        TextView tvContent;
    }
}
