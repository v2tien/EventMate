package com.eventmate.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.eventmate.echelon.BaseActivity;
import com.eventmate.echelon.R;
import com.eventmate.impl.IAdapterListenner;
import com.eventmate.manager.DataStore;
import com.eventmate.models.Session;
import com.eventmate.models.SessionTitleIndex;
import com.eventmate.utils.LogUtils;

import java.util.ArrayList;

/**
 * Created by ADMIN on 3/25/2016.
 */
public class SessionAdapter extends BaseAdapter {

    BaseActivity context;
    ArrayList<Session> listSession;
    LayoutInflater inflater;
    int focusPosition = -1;
    IAdapterListenner adapterListenner;

    public SessionAdapter(BaseActivity context, ArrayList<Session> listSession, IAdapterListenner adapterListenner) {
        this.context = context;
        this.listSession = listSession;
        inflater = LayoutInflater.from(context);
        this.adapterListenner = adapterListenner;
    }

    @Override
    public int getCount() {
        return listSession.size();
    }

    @Override
    public Session getItem(int position) {
        return listSession.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Session session = getItem(position);
        ViewHolder holder = null;
        ViewHolderContent holderContent = null;
        if (convertView == null) {
            if (session instanceof SessionTitleIndex) {
                convertView = inflater.inflate(R.layout.item_session_header, parent, false);
                holder = new ViewHolder();
                holder.tvHeader = (TextView) convertView.findViewById(R.id.tv_header);

                convertView.setTag(holder);
            } else {
                holderContent = new ViewHolderContent();
                convertView = inflater.inflate(R.layout.item_session, parent, false);
                holderContent.tvName = (TextView) convertView.findViewById(R.id.tv_line_1);
                holderContent.tvTime = (TextView) convertView.findViewById(R.id.tv_time);
                holderContent.tvAttendees = (TextView) convertView.findViewById(R.id.tv_line_3);
                holderContent.tvDescription = (TextView) convertView.findViewById(R.id.tv_line_2);
                holderContent.ivBookmarked = (TextView)convertView.findViewById(R.id.tv_bookmarked);
                LogUtils.logInfo("Focus = " + focusPosition + "; pos = " + position);
                convertView.setTag(holderContent);
            }
        } else {
            /*if(session instanceof SessionTitleIndex) {
                holder = (ViewHolder) convertView.getTag();
            }else{
                holderContent = (ViewHolderContent)convertView.getTag();
            }*/
            if (convertView.getTag() instanceof ViewHolder) {
                holder = (ViewHolder) convertView.getTag();
            } else if (convertView.getTag() instanceof ViewHolderContent) {
                holderContent = (ViewHolderContent) convertView.getTag();
            }
        }
        if (session instanceof SessionTitleIndex) {
            if (holder != null && holder.tvHeader != null) {
                holder.tvHeader.setText(((SessionTitleIndex) session).getInfo());
            }
        } else {
            if (holderContent != null && holderContent.tvTime != null && holderContent.tvName != null&& holderContent.tvAttendees!=null&&holderContent.tvDescription!=null) {
                holderContent.tvName.setText(session.getTitle());
                holderContent.tvTime.setText(session.getStart_at().substring(11, 16) + session.getM());
                holderContent.tvAttendees.setText(session.getTotal_attendee() + " attending");
                holderContent.tvDescription.setText(session.getDescription());
            }
            if(session.is_bookmark()){
                holderContent.ivBookmarked.setVisibility(View.VISIBLE);
                convertView.setBackgroundResource(R.drawable.bg_item_session);
            }else{
                holderContent.ivBookmarked.setVisibility(View.INVISIBLE);
                convertView.setBackgroundResource(R.drawable.bg_item_session_unselect);

            }
            //LogUtils.logInfo("session title:"+session.getTitle());
            /*if (position == focusPosition) {
                convertView.setBackgroundResource(R.drawable.bg_item_session);
            } else {
                convertView.setBackgroundResource(R.drawable.bg_item_session_unselect);
            }*/

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    adapterListenner.onClickItemAdapter(position);
                }
            });
        }
        return convertView;
    }

    @Override
    public int getItemViewType(int position) {
        Session session = getItem(position);
        if (session instanceof SessionTitleIndex) {
            return 0;
        } else {
            return 1;
        }

    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    /*@Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Session session = getItem(position);
        ViewHolder holder;
        if(convertView == null){
            if(session instanceof SessionTitleIndex){
                convertView = inflater.inflate(R.layout.item_session_header, parent, false);
                holder = new ViewHolder();
                holder.tvHeader = (TextView)convertView.findViewById(R.id.tv_header);

                convertView.setTag(holder);
            }else{
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.item_session, parent, false);
                holder.tvName = (TextView)convertView.findViewById(R.id.tv_line_1);
                holder.tvTime = (TextView)convertView.findViewById(R.id.tv_time);
                LogUtils.logInfo("Focus = "+focusPosition+"; pos = "+position);

                convertView.setTag(holder);
            }
        }else{
            holder = (ViewHolder)convertView.getTag();
        }
        if(session instanceof SessionTitleIndex){
            if(holder.tvHeader!=null) {
                holder.tvHeader.setText(((SessionTitleIndex) session).getInfo());
            }
        }else{
            if(holder.tvTime!=null && holder.tvName!=null) {
                holder.tvName.setText(session.getTitle());
                holder.tvTime.setText(session.getStart_at().substring(11, 16) + session.getM());
            }
            //LogUtils.logInfo("session title:"+session.getTitle());
            if(position == focusPosition){
                convertView.setBackgroundResource(R.drawable.bg_item_session);
            }else{
                convertView.setBackgroundResource(R.drawable.bg_item_session_unselect);
            }
        }
        return convertView;
    }*/

    public int getFocusPosition() {
        return focusPosition;
    }

    public void setFocusPosition(int focusPosition) {
        this.focusPosition = focusPosition;
    }

    public ArrayList<Session> getListSession() {
        return listSession;
    }

    public void setListSession(ArrayList<Session> listSession) {
        this.listSession = listSession;
    }

    private static class ViewHolder {
        TextView tvHeader;

    }

    private static class ViewHolderContent {
        TextView tvName, tvTime, tvAttendees, tvDescription;
        TextView ivBookmarked;
    }
    /*private static class ViewHolder{
        TextView tvHeader;
        TextView tvName, tvTime;
    }*/
}
