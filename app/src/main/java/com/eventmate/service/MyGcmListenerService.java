package com.eventmate.service;

import android.os.Bundle;

import com.azstack.AzStackClient;
import com.eventmate.utils.Common;
import com.google.android.gms.gcm.GcmListenerService;

/**
 * Created by VuVan on 01/04/2016.
 */
public class MyGcmListenerService extends GcmListenerService {

    @Override
    public void onMessageReceived(String from, Bundle data) {
        AzStackClient azStackClient = AzStackClient.newInstance(Common.context, Common.appId, Common.publicKey);
        azStackClient.showNotification(this, data);
//        EchelonApplication.getInstance().connectAzstackClient(azStackClient);
    }
}
