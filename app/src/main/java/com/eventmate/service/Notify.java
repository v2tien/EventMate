package com.eventmate.service;

/**
 * Created by VuVan on 29/03/2016.
 */
public enum Notify {

    APP_STATUS("com.eventmate.echelon.app.status"), LOGIN_SUCESS("com.eventmate.echelon.notify.login.success");

    private String value;

    private Notify(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
