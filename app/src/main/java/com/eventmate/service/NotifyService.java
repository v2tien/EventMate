package com.eventmate.service;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.eventmate.utils.Common;

/**
 * Created by VuVan on 29/03/2016.
 */
public class NotifyService {

    public static void notifyAppStatus(int status) {
        Intent intent = new Intent(Notify.APP_STATUS.getValue());
        intent.putExtra("status", status);
        LocalBroadcastManager.getInstance(Common.context).sendBroadcast(intent);
    }
}
