package com.eventmate.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.eventmate.models.Contact;
import com.eventmate.models.Session;

import java.util.List;

public class DBHelper extends SQLiteOpenHelper{

    private static final String DB_NAME = "EVENT_MATE";
    private static final int DB_VERSION = 1;
    private static final String CREATE_TABLE_SESSION = "create table Session (id integer primary key, data text)";
    private static final String CREATE_TABLE_CONTACT = "create table Contact (id integer primary key, data text)";

    public DBHelper(Context context){
        super(context,DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_SESSION);
        db.execSQL(CREATE_TABLE_CONTACT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists Session");
        db.execSQL("drop table if exists Contact");
        onCreate(db);
    }

    public boolean checkSessionExist(Session session){
        String query = "select id from Contact where id = "+session.getSession_id();
        Cursor cursor = getReadableDatabase().rawQuery(query, null);
        if(cursor.getCount()>0){
            return true;
        }else{
            return false;
        }
    }

    public boolean checkContactExist(Contact contact){
        String query = "select id from Session where id = "+contact.getContact_id();
        Cursor cursor = getReadableDatabase().rawQuery(query, null);
        if(cursor.getCount()>0){
            return true;
        }else{
            return false;
        }
    }

    public void addSessions(List<Session> sessions){
        ContentValues cv = new ContentValues();
        for(Session session:sessions){
            cv.clear();
            cv.put("id", session.getSession_id());
            cv.put("data", session.toString());
            getWritableDatabase().insert("Session", null, cv);
        }
    }

    public void addContact(List<Contact> contacts){
        ContentValues cv = new ContentValues();
        for(Contact contact:contacts){
            cv.clear();
            cv.put("id", contact.getContact_id());
            cv.put("data", contact.toString());
            getWritableDatabase().insert("Contact", null, cv);
        }
    }
}
