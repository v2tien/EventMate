package com.eventmate.echelon;

import android.content.Intent;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.eventmate.utils.LogUtils;

public class DVWebviewActivity extends BaseActivity{

    WebView wv;
    String url;
    ProgressBar loadingBar;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_dvwebview;
    }

    @Override
    protected void initView() {
        loadingBar = (ProgressBar)findViewById(R.id.pb_load_web_progress);
        wv = (WebView)findViewById(R.id.wv);
        wv.setWebViewClient(new MyBrowser());
        wv.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onProgressChanged(WebView view, int progress) {
                if (progress < 100
                        && loadingBar.getVisibility() == ProgressBar.GONE) {
                    loadingBar.setVisibility(ProgressBar.VISIBLE);
                }
                loadingBar.setProgress(progress);
                if (progress == 100) {
                    loadingBar.setVisibility(ProgressBar.GONE);
                }
            }
        });
        getData();
    }

    private void getData(){
        Intent intent = getIntent();
        url = intent.getStringExtra(WebviewActivity.KEY_URL);
        LogUtils.logInfo("URL="+url);
        wv.loadUrl(url);
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && wv.canGoBack()) {
            Log.e("onKeyDown", "true");
            wv.goBack();
            return true;
        } else {
            Log.e("false", "false");
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
