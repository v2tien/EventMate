package com.eventmate.echelon;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.eventmate.adapter.EventChooseAdapter;
import com.eventmate.customview.DividerItemDecoration;
import com.eventmate.impl.IAdapterListenner;
import com.eventmate.manager.ApiResponse;
import com.eventmate.manager.DataStore;
import com.eventmate.models.Event;
import com.eventmate.models.User;
import com.eventmate.utils.Common;
import com.eventmate.utils.ConnectionUtils;
import com.eventmate.utils.Constant;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

public class EventChoseActivity extends BaseActivity implements IAdapterListenner, View.OnClickListener {

    public static final String KEY_CODE_USER = "CODE_USER";

    private TextView tvName, tvLogout;
    private RecyclerView rvEvent;
    private List<Event> eventList;
    private ImageView imgAvatar;

    private String code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        eventList = EchelonApplication.getInstance().getListEvent();
        getExtraData();
        setAdapter();
    }

    private void getExtraData() {
        if (getIntent().hasExtra(KEY_CODE_USER)){
            code = getIntent().getStringExtra(KEY_CODE_USER);
        }else finish();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_event_chose;
    }

    @Override
    protected void initView() {
        tvName = (TextView) findViewById(R.id.tv_name);
        tvName.setText(DataStore.getInstance().getUserName());
        tvLogout = (TextView) findViewById(R.id.tv_logout);
        rvEvent = (RecyclerView) findViewById(R.id.rcv_event);
        imgAvatar = (ImageView)findViewById(R.id.img_avartar);
        String avatar = DataStore.getInstance().getUserAvatar();
        if(avatar == null || avatar.length() == 0 || avatar.equals("null")) {
            Picasso.with(this).load(R.drawable.default_avatar).into(imgAvatar);
        }else{
            Picasso.with(this).load(avatar).into(imgAvatar);
        }

        rvEvent.addItemDecoration(new DividerItemDecoration(this, R.drawable.ic_line_horizontal));
        rvEvent.setHasFixedSize(true);
        rvEvent.setLayoutManager(new LinearLayoutManager(this));

        tvLogout.setOnClickListener(this);
    }

    private void showDialogConfirmLogout() {
        new AlertDialog.Builder(this).setMessage(R.string.logout_confirm).setTitle(R.string.app_name)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EchelonApplication.getInstance().setToken("");
                        //DataStore.getInstance().saveUser(null);
//                        AzStackClient.getInstance().logout();
                        DataStore.getInstance().clearLoginData();
                        DataStore.getInstance().clearLoginCode();
                        DataStore.getInstance().logout();

                        gotoActivity(LoginActivity.class);
                        finish();
                    }
                }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setCancelable(true).show();
    }


    @Override
    public void onClick(View v) {
        if (v == tvLogout){
           showDialogConfirmLogout();
        }
    }

    @Override
    public void onClickItemAdapter(int position) {
        String evenId = eventList.get(position).getEvent_id();
        // save event
        EchelonApplication.getInstance().setEvent(eventList.get(position));
        checkEventCode(code, evenId);
    }

    private void setAdapter(){
        final EventChooseAdapter eventChooseAdapter = new EventChooseAdapter(eventList,this,this);
        rvEvent.setAdapter(eventChooseAdapter);
    }

    private void checkEventCode(String code, String eventId){
        if (!ConnectionUtils.checkConnection(this)){
            showToast(R.string.network_no);
            return;
        }
        showProgressDialog(R.string.loading);
        ConnectionUtils.login(code,eventId,new JsonHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response){
                hideProgressDialog();
                ApiResponse apiResponse =  new ApiResponse(response);
                if (!apiResponse.isError()) {
                    try {
                        String token = response.getString(Constant.Key.KEY_TOKEN);
                        if (!token.equals(Constant.Key.KEY_FALSE)){
                            // save user
                            List<User> users = apiResponse.getResponseObjectList(User.class);
                            User user = users.get(0);
                            EchelonApplication.getInstance().setUser(user);
                            //DataStore.getInstance().saveUser(user);
                            // save token
                            EchelonApplication.getInstance().setToken(token);

                            //login sdk
                            Common.token = token;
                            Common.userId = Integer.parseInt(user.getContact_id());
                            Common.name = user.getFirst_name()+" "+user.getLast_name();

                            EchelonApplication.initAzStack();


                            gotoActivity(HomeActivity.class);
                            finish();
                        }else showToast(apiResponse.getMessage());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }else{
                    showAlertMessage(apiResponse.getMessage());
                }

            }


        });
    }


}
