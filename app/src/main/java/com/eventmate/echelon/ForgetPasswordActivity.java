package com.eventmate.echelon;

import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eventmate.manager.ApiResponse;
import com.eventmate.utils.ConnectionUtils;
import com.eventmate.utils.Constant;
import com.eventmate.utils.StringUtils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Trang on 3/28/2016.
 */
public class ForgetPasswordActivity extends BaseActivity implements View.OnClickListener {

    private EditText edtCode;
    private TextView tvLabel, tvForgot;
    private Toolbar toolbar;
    private RelativeLayout loRoot;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initView() {
        initToolbar();
        edtCode = (EditText) findViewById(R.id.edt_code);
        tvLabel = (TextView) findViewById(R.id.tv_label);
        tvForgot = (TextView) findViewById(R.id.tv_forget_pass);
        loRoot = (RelativeLayout) findViewById(R.id.root_view);
        loRoot.setOnClickListener(this);

        tvForgot.setVisibility(View.GONE);
        tvLabel.setText(getString(R.string.welcome_edt_hint_email));
        edtCode.setText("");
        edtCode.setInputType( InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS );
        edtCode.setHint(R.string.email_example);

        edtCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO
                        || actionId == EditorInfo.IME_ACTION_DONE) {
                    String email = edtCode.getText().toString();
                    if (email.length() == 0)
                        return false;

                    if (StringUtils.isValidEmail(email)) {
                        checkEmail(email);
                    }else{
                        showToast(R.string.email_invalid);
                    }
                    //gotoActivity(SessionDetailActivity.class);
                    return true;
                }
                return false;
            }
        });

    }

    private void initToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.welcome_forgot_password);
        toolbar.showOverflowMenu();
        //toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void checkEmail(String email) {
        if (!ConnectionUtils.checkConnection(this)){
            showToast(R.string.network_no);
            return;
        }
        showProgressDialog(R.string.loading);
        RequestParams params = new RequestParams();
        params.put(Constant.Key.KEY_EMAIL,email);
        ConnectionUtils.get(Constant.Url.URL_USER_FORGOT_PASSWORD, params, new JsonHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                hideProgressDialog();
                ApiResponse apiResponse = new ApiResponse(response);
                if (!apiResponse.isError()){
                    showLongToast(R.string.sent_password);
                    finish();
                }else showToast(apiResponse.getMessage());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                hideProgressDialog();
            }
        });
    }


    @Override
    public void onClick(View v) {
        if (v == loRoot){
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(edtCode.getWindowToken(), 0);
        }
    }
}
