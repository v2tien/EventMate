package com.eventmate.echelon;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.eventmate.utils.DeviceUtils;

public abstract class BaseActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DeviceUtils.getScreenSize(this);
        setContentView(getLayoutId());
        initView();
    }

    public void makeToast(String msg) {
        if (msg != null) {
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "null", Toast.LENGTH_LONG).show();
        }
    }

    public void makeToast(int msgId) {
        String msg = getString(msgId);
        makeToast(msg);
    }



    public void showProgressDialog(String message){
        if(progressDialog==null){
            progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);
        }
        progressDialog.setMessage(message == null?"Null":message);
        progressDialog.show();
    }

    public void showProgressDialog(int messageId){
        String message = getString(messageId);
        showProgressDialog(message);
    }

    public void hideProgressDialog(){
        if(progressDialog!=null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
    }

    public void showAlertMessage(String message) {
        new AlertDialog.Builder(this).setTitle(R.string.app_name).setMessage(message == null?"null":message)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).setCancelable(true).show();
    }

    public void showAlertMessage(int messageId){
        String message = getString(messageId);
        showAlertMessage(message);
    }

    public void showToast(String message){
        Toast.makeText(BaseActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    public void showToast(int message){
        Toast.makeText(BaseActivity.this, getString(message), Toast.LENGTH_SHORT).show();
    }

    public void showLongToast(int message){
        Toast.makeText(BaseActivity.this, getString(message), Toast.LENGTH_LONG).show();
    }

    public void gotoActivity(Class T){
        Intent intent = new Intent(this, T);
        startActivity(intent);
    }

    public void openWebpage(String url){
        try {
            if (url == null || url.equals("")){
                showLongToast(R.string.no_content);
            }else {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void initToolbar(String title){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundResource(R.color.session_colorPrimary);
        setSupportActionBar(toolbar);
        if (title != null && !title.isEmpty()) {
            getSupportActionBar().setTitle(title);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //toolbar.showOverflowMenu();
        //toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    protected abstract int getLayoutId();

    protected abstract void initView();

}
