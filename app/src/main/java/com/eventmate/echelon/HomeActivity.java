package com.eventmate.echelon;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.azstack.AzStackClient;
import com.eventmate.adapter.MenuLeftAdapter;
import com.eventmate.adapter.PagesAdapter;
import com.eventmate.customview.SmartEditText;
import com.eventmate.fragments.AgendaFragment;
import com.eventmate.fragments.AttendeesFragment;
import com.eventmate.fragments.WebViewFragment;
import com.eventmate.fragments.WebViewTabFragment;
import com.eventmate.manager.DataStore;
import com.eventmate.models.Annoucement;
import com.eventmate.models.ContactType;
import com.eventmate.models.LeftMenuItem;
import com.eventmate.models.User;
import com.eventmate.service.Notify;
import com.eventmate.utils.Common;
import com.eventmate.utils.ConnectionUtils;
import com.eventmate.utils.Constant;
import com.eventmate.utils.DeviceUtils;
import com.eventmate.utils.LogUtils;
import com.google.gson.Gson;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends BaseActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {

    private SlidingMenu menu;
    private ImageView btAction;
    private ViewPager vpContent;
    private ImageView[] tabIcons;
    private TextView[] tabTitles;
    private ImageView[] tabBorders;
    private Button searchView;
    private SmartEditText searchView2;
    private TextView tvTitle;
    private ImageView btSelectEvent;
    private View layoutNavigation, layoutDarkBg, vConnect;
    private TextView tvConnect;
    private Dialog dialogListEventFull;
    private Dialog dialogSugguestSearch;
    private static final int[] tabIconIds = {R.id.tab_message_icon, R.id.tab_attendees_icon, R.id.tab_agenda_icon, R.id.tab_map_icon, R.id.tab_information_icon};
    private static final int[] tabTitleIds = {R.id.tab_message_title, R.id.tab_attendees_title, R.id.tab_agenda_title, R.id.tab_map_title, R.id.tab_information_title};
    private static final int[] tabDrawableFocus = {R.drawable.ic_message_focus, R.drawable.ic_attende_focus, R.drawable.ic_agenda_focus, R.drawable.ic_map_focus, R.drawable.ic_information_focus};
    private static final int[] tabDrawableUnfocus = {R.drawable.ic_message_unfocus, R.drawable.ic_attendees_unfocus, R.drawable.ic_agenda_click, R.drawable.ic_map_click, R.drawable
            .ic_information_unfocus};
    private static final int[] tabBorderIds = {R.id.iv_border_tab_0, R.id.iv_border_tab_1, R.id.iv_border_tab_2, R.id.iv_border_tab_3, R.id.iv_border_tab_4};
    private static final int[] tabIds = {R.id.layout_tab_message, R.id.layout_tab_attendees, R.id.layout_tab_agenda, R.id.layout_tab_map, R.id.layout_tab_info};
    public static int searchViewWidth, searchViewHeight;
    private View layoutSearch;

    private BroadcastReceiver checkNetworkReceiver;
    private BroadcastReceiver appStatusReceiver;
    // left menu
    MenuLeftAdapter menuAdapter;
    ArrayList<LeftMenuItem> listMenuItems;

    //mode
    private static final int MODE_SEARCH_CONTACT = 1;
    private static final int MODE_FILTER = -1;
    int mode = -1;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_home;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EchelonApplication.homeActivity = this;
        loadAllContactType();
        addCustomMenu();
        if (AttendeesFragment.getInstance().isFirstTime() || AttendeesFragment.getInstance().getFocusContactType() == null) {
            searchContactByType(null);
        }

        EchelonApplication.connect();
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        return super.onCreateView(name, context, attrs);

    }

    @Override
    protected void initView() {
        tvConnect = (TextView) findViewById(R.id.tv_no_connection);
        vConnect = findViewById(R.id.v_connect);
        btAction = (ImageView) findViewById(R.id.bt_action);
        btAction.setOnClickListener(this);
        vpContent = (ViewPager) findViewById(R.id.vp_content);
        vpContent.setOnPageChangeListener(this);
        vpContent.setOffscreenPageLimit(4);
        PagesAdapter adapter = new PagesAdapter(getSupportFragmentManager());
        vpContent.setAdapter(adapter);
        tabBorders = new ImageView[tabBorderIds.length];
        tabIcons = new ImageView[tabIconIds.length];
        tabTitles = new TextView[tabTitleIds.length];
        for (int i = 0; i < tabIconIds.length; i++) {
            tabIcons[i] = (ImageView) findViewById(tabIconIds[i]);
            tabTitles[i] = (TextView) findViewById(tabTitleIds[i]);
            tabBorders[i] = (ImageView) findViewById(tabBorderIds[i]);
            //tabIcons[i].setOnClickListener(this);
            findViewById(tabIds[i]).setOnClickListener(this);
        }
        layoutNavigation = findViewById(R.id.layout_search);
        layoutNavigation.post(new Runnable() {
            @Override
            public void run() {
                EchelonApplication.getInstance().navigationHeight = layoutNavigation.getHeight();
            }
        });
        layoutDarkBg = findViewById(R.id.layout_dark_bg);
        layoutDarkBg.setOnClickListener(this);
        layoutDarkBg.setVisibility(View.GONE);
        searchView = (Button) findViewById(R.id.et_search);
        //searchView.setOnFocusChangeListener(this);
        searchView.setOnClickListener(this);


        searchView.post(new Runnable() {
            @Override
            public void run() {
                searchViewWidth = searchView.getWidth();
                searchViewHeight = searchView.getHeight();
            }
        });
        searchView2 = (SmartEditText) findViewById(R.id.et_search_2);
        searchView2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String name = searchView2.getText().toString();
                LogUtils.logInfo("name=" + name);
                if (!name.isEmpty()) {
                    AttendeesFragment.filterText = "Search \'" + name + "\' result";
                    searchView.setText(name);
                } else {
                    searchView.setText("All attendees");
                }
                if (vpContent.getCurrentItem() == 1) {
                    AttendeesFragment.getInstance().searchContactByName(name);
                    AttendeesFragment.getInstance().changeHeader();
                }
            }
        });
        searchView2.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                DeviceUtils.hideKeyboard(HomeActivity.this, searchView2);
                searchView2.setVisibility(View.GONE);
                searchView.setVisibility(View.VISIBLE);
                return true;
            }
        });
        searchView2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    searchView2.setVisibility(View.GONE);
                    searchView.setVisibility(View.VISIBLE);
                    DeviceUtils.hideKeyboard(HomeActivity.this, searchView2);
                } else {

                }
            }
        });
        tvTitle = (TextView) findViewById(R.id.tv_title);
        btSelectEvent = (ImageView) findViewById(R.id.bt_select_event);
        btSelectEvent.setOnClickListener(this);
        layoutSearch = findViewById(R.id.layout_search);
        onTabClick(2);
        initMenu();
        registerBroadCastReceiver();

    }

    private void initMenu() {
        menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        menu.setFadeDegree(0.35f);
        menu.setShadowWidth(50);
        int screenWidth = DeviceUtils.getScreenSize(this)[0];
        menu.setBehindOffset(screenWidth / 4);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.menu_left);
        CircleImageView ivAva = (CircleImageView) menu.findViewById(R.id.iv_avatar);

        String image = EchelonApplication.getInstance().getUser().getImg();
        if (image == null || image.equals("null") || image.length() == 0) {
            Picasso.with(this).load(R.drawable.default_avatar).into(ivAva);
        }else{
            Picasso.with(this).load(EchelonApplication.getInstance().getUser().getImg()).into(ivAva);
        }

        ListView lvItem = (ListView) menu.findViewById(R.id.lv_item);
        listMenuItems = new ArrayList<LeftMenuItem>();
        listMenuItems.add(new LeftMenuItem(R.drawable.ic_speaker, getString(R.string.annoucement), LeftMenuItem.TYPE_NORMAL));
        listMenuItems.add(new LeftMenuItem(R.drawable.ic_agenda, getString(R.string.agenda), LeftMenuItem.TYPE_NORMAL));
        menuAdapter = new MenuLeftAdapter(this, listMenuItems);
        lvItem.setAdapter(menuAdapter);
        menuAdapter.setSelectedPos(1);
        menuAdapter.notifyDataSetChanged();
        lvItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                menu.toggle(true);
                LeftMenuItem leftMenuItem = menuAdapter.getItem(position);
                if (position != menuAdapter.getSelectedPos()) {
                    menuAdapter.setSelectedPos(position);
                    menuAdapter.notifyDataSetChanged();
                }
                if (leftMenuItem.getPage_url() != null) {
                    LogUtils.logInfo("leftMenuItem.getPage_url()=" + leftMenuItem.getPage_url());
                    Intent intent = new Intent(HomeActivity.this, DVWebviewActivity.class);
                    //Intent intent = new Intent(HomeActivity.this, AnnoucementActivity.class);
                    intent.putExtra(WebviewActivity.KEY_URL, leftMenuItem.getPage_url());
                    startActivity(intent);
                    return;
                }
                switch (position) {
                    case 0:
                        getListAnnouncement();
                        break;
                    case 1:
                        onTabClick(2);
                        break;
                }
            }
        });
        View logoutView = menu.findViewById(R.id.layout_footer_menu);
        TextView tvUserName = (TextView) menu.findViewById(R.id.tv_username);
        User user = EchelonApplication.getInstance().getUser();
        if (user.getFirst_name() != null && user.getLast_name() != null) {
            tvUserName.setText(user.getFirst_name() + " " + user.getLast_name());
        }
        CircleImageView ivAvatar = (CircleImageView) menu.findViewById(R.id.iv_avatar);
        if (user.getImg() == null || user.getImg().length() == 0 || user.getImg().equals("null")) {
            Picasso.with(this).load(R.drawable.default_avatar).into(ivAvatar);
        }else{
            Picasso.with(this).load(user.getImg()).into(ivAvatar);
        }
        ivAvatar.setOnClickListener(this);
        TextView tvCompany = (TextView) menu.findViewById(R.id.tv_company);
        if (user.getCompany() != null && !user.getCompany().isEmpty()) {
            tvCompany.setText(user.getCompany());
        }
        logoutView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogConfirmLogout();
            }
        });

    }

    private void addCustomMenu() {
        JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                LogUtils.logInfo("Custom menu:" + response.toString());
                try {
                    boolean success = response.getBoolean(Constant.Key.KEY_SUCCESS);
                    if (success) {
                        Gson gson = new Gson();
                        JSONArray arrData = response.getJSONArray(Constant.Key.KEY_DATA);
                        for (int i = 0; i < arrData.length(); i++) {
                            LeftMenuItem item = gson.fromJson(arrData.getJSONObject(i).toString(), LeftMenuItem.class);
                            listMenuItems.add(item);
                        }
                        ListView lvLeftMenu = (ListView) menu.findViewById(R.id.lv_item);
                        MenuLeftAdapter adapter = (MenuLeftAdapter) lvLeftMenu.getAdapter();
                        adapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                ConnectionUtils.logOut(HomeActivity.this);
            }
        };
        if (ConnectionUtils.checkConnection(this)) {
            ConnectionUtils.getCustomMenu(EchelonApplication.getInstance().getToken(), handler);
        }

    }

    private void openWebPageActivity(String url, String title) {
        Intent intent = new Intent(this, WebviewActivity.class);
        intent.putExtra(WebviewActivity.KEY_URL, url);
        intent.putExtra(WebviewActivity.KEY_TITLE, title);

        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        int currentTabPos = vpContent.getCurrentItem();
        if (menu.isMenuShowing()) {
            menu.showContent(true);
        } else if (currentTabPos == 3) {
            if (WebViewTabFragment.webView.canGoBack()) {
                WebViewTabFragment.webView.goBack();
            } else {
                showDialogConfirmExit();
            }
        } else if (currentTabPos == 4) {
            if (WebViewFragment.wvContent.canGoBack()) {
                WebViewFragment.wvContent.goBack();
            } else {
                showDialogConfirmExit();
            }
        } else {
            showDialogConfirmExit();
            //finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_avatar:
                gotoActivity(UpdateInfoActivity.class);
                break;
            case R.id.bt_action:
                menu.toggle(true);
                break;
            case R.id.layout_tab_message:
                onTabClick(0);
                break;
            case R.id.layout_tab_attendees:
                onTabClick(1);
                break;
            case R.id.layout_tab_agenda:
                onTabClick(2);
                break;
            case R.id.layout_tab_map:
                onTabClick(3);
                break;
            case R.id.layout_tab_info:
                onTabClick(4);
                break;
            case R.id.et_search:
                LogUtils.logInfo("click search view");
                int currentTabPos = vpContent.getCurrentItem();
                if (currentTabPos == 0 || currentTabPos == 1 || currentTabPos == 3 || currentTabPos == 4) {
                    //showPopupSugguestSearch();
                    //AttendeesFragment.getInstance().showPopupSugguestSearch();
                    AttendeesFragment.getInstance().showPopupContactType();

                } else if (currentTabPos == 2) {
                    //AgendaFragment.getInstance().showDialogSugguestSearch();
                    AgendaFragment.getInstance().showPopupSessionFilter();
                }
                break;
            case R.id.bt_select_event:
                LogUtils.logInfo("vpContent.getCurrentItem() = " + vpContent.getCurrentItem());
                if (vpContent.getCurrentItem() == 1) {
                    //searchContactByType(AttendeesFragment.getInstance().getFocusContactType());
                    searchView2.setVisibility(View.VISIBLE);
                    searchView2.setText("");
                    searchView2.requestFocus();
                    DeviceUtils.showSoftKeyboard(HomeActivity.this);
                    searchView.setVisibility(View.GONE);
                } else if (vpContent.getCurrentItem() == 2) {
                    //AgendaFragment.getInstance().loadAllSession();
                    AgendaFragment.getInstance().getCurrentSession();
                }
                break;
            case R.id.layout_dark_bg:
                if (layoutDarkBg.getVisibility() == View.VISIBLE) {
                    layoutDarkBg.setVisibility(View.GONE);
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //DeviceUtils.hideKeyboard(this, searchView);

        //Status connect sdk
        if (Common.appStatus == Constant.Key.APP_STATUS_UPDATING) {
            vConnect.setVisibility(View.VISIBLE);
            tvConnect.setText(R.string.updating);
        } else if (Common.appStatus == Constant.Key.APP_STATUS_UPDATING_COMPLETE) {
            vConnect.setVisibility(View.GONE);
        } else {
            if (!ConnectionUtils.checkConnection(this)) {
                if (tvConnect != null && vConnect != null ||
                        Common.appStatus == Constant.Key.APP_STATUS_WAITING) {
                    tvConnect.setText(R.string.waiting_for_network);
                    vConnect.setVisibility(View.VISIBLE);
                }
            } else if (!AzStackClient.getInstance().isAuthenticated() ||
                    Common.appStatus == Constant.Key.APP_STATUS_CONNECTING) {
                if (tvConnect != null && vConnect != null) {
                    tvConnect.setText(R.string.azstack_connecting);
                    vConnect.setVisibility(View.GONE);
                }
            }
        }
        if (vpContent.getCurrentItem() == 2) {
            AgendaFragment.getInstance().notifiDatasetchanged();
            menuAdapter.setSelectedPos(1);
            menuAdapter.notifyDataSetChanged();
        } else {
            menuAdapter.setSelectedPos(-1);
            menuAdapter.notifyDataSetChanged();
        }
    }

    public void onTabClick(int position) {
        DeviceUtils.hideKeyboard(this, searchView2);
        for (int i = 0; i < tabIcons.length; i++) {
            if (i == position) {
                tabIcons[i].setImageResource(tabDrawableFocus[i]);
                tabTitles[i].setTextColor(getResources().getColor(android.R.color.white));
                tabBorders[i].setVisibility(View.VISIBLE);
            } else {
                tabIcons[i].setImageResource(tabDrawableUnfocus[i]);
                tabTitles[i].setTextColor(getResources().getColor(R.color.color_unfocused));
                tabBorders[i].setVisibility(View.INVISIBLE);
            }
        }
        vpContent.setCurrentItem(position);
        if (menuAdapter != null) {
            if (position == 2) {
                menuAdapter.setSelectedPos(1);
            } else {
                menuAdapter.setSelectedPos(-1);
            }
            menuAdapter.notifyDataSetChanged();
        }
        /*if(position == 1|| position == 2){
            layoutSearch.setVisibility(View.VISIBLE);
        }else{
            layoutSearch.setVisibility(View.GONE);
        }*/
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        onTabClick(position);
        //Attendees
        if (position == 0) {
            searchView.setVisibility(View.GONE);
            searchView2.setVisibility(View.GONE);
            LogUtils.logInfo("invisible = " + Boolean.toString(searchView.getVisibility() == View.GONE));
            searchView.setVisibility(View.GONE);
            LogUtils.logInfo("invisible = " + Boolean.toString(searchView.getVisibility() == View.GONE));
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText("Message");
            btSelectEvent.setVisibility(View.INVISIBLE);
        } else if (position == 1) {
            btSelectEvent.setVisibility(View.VISIBLE);
            btSelectEvent.setImageResource(R.drawable.ic_search);
//            if(AttendeesFragment.getInstance().isFirstTime()&&AttendeesFragment.getInstance().getFocusContactType() == null){
//                searchContactByType(null);
//            }
            searchView.setVisibility(View.VISIBLE);
            tvTitle.setVisibility(View.GONE);
            searchView.setText(AttendeesFragment.getInstance().getFilterText());
            AttendeesFragment.getInstance().changeHeader();

        } else if (position == 2) {
            searchView.setVisibility(View.VISIBLE);
            tvTitle.setVisibility(View.GONE);
            btSelectEvent.setVisibility(View.VISIBLE);
            btSelectEvent.setImageResource(R.drawable.ic_tivi);
            searchView.setText(AgendaFragment.getInstance().getFilterText());
        } else if (position == 3) {
            btSelectEvent.setVisibility(View.INVISIBLE);
            searchView.setVisibility(View.GONE);
            searchView2.setVisibility(View.GONE);
            LogUtils.logInfo("invisible = " + Boolean.toString(searchView.getVisibility() == View.GONE));
            searchView.setVisibility(View.GONE);
            LogUtils.logInfo("invisible = " + Boolean.toString(searchView.getVisibility() == View.GONE));
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText("Map");
        } else if (position == 4) {
            btSelectEvent.setVisibility(View.INVISIBLE);
            searchView.setVisibility(View.GONE);
            searchView2.setVisibility(View.GONE);
            LogUtils.logInfo("invisible = " + Boolean.toString(searchView.getVisibility() == View.GONE));
            searchView.setVisibility(View.GONE);
            LogUtils.logInfo("invisible = " + Boolean.toString(searchView.getVisibility() == View.GONE));
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText("Information");
        }

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    private void loadAllContactType() {
        if (EchelonApplication.getInstance().getListContactType().isEmpty() && ConnectionUtils.checkConnection(this)) {
            JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    LogUtils.logInfo("Get contact type:" + response.toString());
                    Gson gson = new Gson();
                    try {
                        boolean success = response.getBoolean(Constant.Key.KEY_SUCCESS);
                        if (success) {
                            JSONArray dataArr = response.getJSONArray(Constant.Key.KEY_DATA);
                            for (int i = 0; i < dataArr.length(); i++) {
                                ContactType contactType = gson.fromJson(dataArr.getJSONObject(i).toString(), ContactType.class);
                                EchelonApplication.getInstance().getListContactType().add(contactType);
                            }
                            EchelonApplication.getInstance().getListContactType().add(0, new ContactType(ContactType.DEFAULT_ID, "All attendees"));
                            EchelonApplication.getInstance().getListContactType().add(1, new ContactType(ContactType.DEFAULT_ID, "Bookmarked"));
                            LogUtils.logInfo("Done load contact type");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            };

            String token = EchelonApplication.getInstance().getToken();
            ConnectionUtils.getContactType(token, handler);
        }
    }

    public void searchContactByType(final ContactType contactType) {
        if (contactType == null || contactType.getContact_type_id() == ContactType.DEFAULT_ID || contactType.getContact_type_id() == ContactType.DEFAULT_ID_BOOKMARKED) {
            if (contactType != null && contactType.getContact_type_id() == ContactType.DEFAULT_ID) {
                AttendeesFragment.filterText = "All attendees";
                if (vpContent.getCurrentItem() == 1) {
                    AttendeesFragment.getInstance().showAllAttendees();
                    AttendeesFragment.getInstance().changeHeader();
                }
            } else if (contactType != null && contactType.getContact_type_id() == ContactType.DEFAULT_ID_BOOKMARKED) {
                AttendeesFragment.filterText = "Bookmarked";
            }
        } else {
            AttendeesFragment.filterText = contactType.getTitle();
        }
        if (vpContent.getCurrentItem() == 1) {
            getSearchView().setText(AttendeesFragment.filterText);
        }
        if (ConnectionUtils.checkConnection(this)) {
//            showProgressDialog(contactType != null ? "Getting contact type " + contactType.getTitle() : "Getting contacts");
            JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    LogUtils.logInfo("response attendees:" + response.toString());
                    hideProgressDialog();
                    if (AttendeesFragment.listContatcs == null) {
                        AttendeesFragment.listContatcs = new ArrayList<User>();
                    }
                    AttendeesFragment.listContatcs.clear();
                    try {
                        boolean success = response.getBoolean(Constant.Key.KEY_SUCCESS);
                        String message = response.getString(Constant.Key.KEY_MESSAGE);
                        if (!success) {
                            showAlertMessage(message);
                        } else {
                            Gson gson = new Gson();
                            JSONArray arrData = response.getJSONArray(Constant.Key.KEY_DATA);
                            if (contactType == null) {
                                AttendeesFragment.getInstance().getListAll().clear();
                            }
                            for (int i = 0; i < arrData.length(); i++) {
                                User user = gson.fromJson(arrData.getJSONObject(i).toString(), User.class);
                                AttendeesFragment.listContatcs.add(user);
                                if (contactType == null) {
                                    AttendeesFragment.getInstance().getListAll().add(user);
                                }
                            }
                            EchelonApplication.getInstance().setListUser(AttendeesFragment.listContatcs);
                            AttendeesFragment.getInstance().showContact();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    hideProgressDialog();
                    ConnectionUtils.logOut(HomeActivity.this);
                }
            };
            ConnectionUtils.getAllContact(EchelonApplication.getInstance().getToken(), contactType, handler);
        } else {
            makeToast(R.string.network_no);
        }

    }

    private void registerBroadCastReceiver() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        checkNetworkReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                if (!ConnectionUtils.checkConnection(context)) {
                    if (tvConnect != null && vConnect != null) {
                        Common.appStatus = Constant.Key.APP_STATUS_WAITING;
                        tvConnect.setText(R.string.azstack_waiting_for_network);
                        vConnect.setVisibility(View.VISIBLE);
                    }
                } else if (!AzStackClient.getInstance().isAuthenticated()) {
                    if (tvConnect != null && vConnect != null) {
                        Common.appStatus = Constant.Key.APP_STATUS_CONNECTING;
                        tvConnect.setText(R.string.azstack_connecting);
                        vConnect.setVisibility(View.VISIBLE);
                    }
                } else {
//                    vConnect.setVisibility(View.GONE);
                }
            }
        };
        registerReceiver(checkNetworkReceiver, filter);

        IntentFilter filter1 = new IntentFilter(Notify.APP_STATUS.getValue());
        appStatusReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                int status = intent.getIntExtra("status", 0);
                if (status == Constant.Key.APP_STATUS_UPDATING) {
                    tvConnect.setText(R.string.updating);
                } else if (status == Constant.Key.APP_STATUS_UPDATING_COMPLETE) {
                    vConnect.setVisibility(View.GONE);
                } else if (ConnectionUtils.checkConnection(getBaseContext()) &&
                        AzStackClient.getInstance().isAuthenticated()) {
//                    tvConnect.setText(R.string.connected);
//                    vConnect.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            vConnect.setVisibility(View.GONE);
//                        }
//                    }, 1000);
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(appStatusReceiver, filter1);
    }

    public Button getSearchView() {
        return searchView;
    }

    public void showDarkBackground() {
        layoutDarkBg.setVisibility(View.VISIBLE);
    }

    public void hideDarkBackground() {
        layoutDarkBg.setVisibility(View.GONE);
    }

    private void getListAnnouncement() {
        LogUtils.logInfo("NUmber anno:" + EchelonApplication.getInstance().getListAnnoucement().size());

        JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                hideProgressDialog();
                try {
                    boolean success = response.getBoolean(Constant.Key.KEY_SUCCESS);
                    if (success) {
                        Gson gson = new Gson();
                        JSONArray arrData = response.getJSONArray(Constant.Key.KEY_DATA);
                        for (int i = 0; i < arrData.length(); i++) {
                            Annoucement annoucement = gson.fromJson(arrData.getJSONObject(i).toString(), Annoucement.class);
                            EchelonApplication.getInstance().getListAnnoucement().add(annoucement);
                        }
                        gotoActivity(AnnoucementActivity.class);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                hideProgressDialog();
                ConnectionUtils.logOut(HomeActivity.this);
            }
        };
        if (EchelonApplication.getInstance().getListAnnoucement().size() > 0) {
            gotoActivity(AnnoucementActivity.class);
        } else {
            if (ConnectionUtils.checkConnection(this)) {
                showProgressDialog("Loading...");
                ConnectionUtils.getListAnnouncement(EchelonApplication.getInstance().getToken(), handler);
            } else {
                makeToast(R.string.network_no);
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AgendaFragment.getInstance().clearData();
        AttendeesFragment.getInstance().clearData();
        EchelonApplication.getInstance().clearData();
    }

    private void showDialogConfirmLogout() {
        new AlertDialog.Builder(this).setMessage(R.string.logout_confirm).setTitle(R.string.app_name)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EchelonApplication.getInstance().setToken("");
                        //DataStore.getInstance().saveUser(null);
                        AzStackClient.getInstance().logout();
                        DataStore.getInstance().clearLoginData();
                        DataStore.getInstance().clearLoginCode();
                        DataStore.getInstance().logout();
                        gotoActivity(LoginActivity.class);
                        finish();
                    }
                }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setCancelable(true).show();
    }

    private void showDialogConfirmExit() {
        new AlertDialog.Builder(this).setMessage(R.string.exit_confirm).setTitle(R.string.app_name)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EchelonApplication.getInstance().setToken("");
                        //DataStore.getInstance().saveUser(null);
//                        AzStackClient.getInstance().logout();
                        finish();
                        System.exit(0);
                    }
                }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setCancelable(true).show();
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        int currentTabPos = vpContent.getCurrentItem();
//        if (currentTabPos == 3) {
//            if ((keyCode == KeyEvent.KEYCODE_BACK) && WebViewTabFragment.webView.canGoBack()) {
//                WebViewTabFragment.webView.goBack();
//                return true;
//            }
//        } else if (currentTabPos == 4) {
//            if ((keyCode == KeyEvent.KEYCODE_BACK) && WebViewFragment.wvContent.canGoBack()) {
//                WebViewFragment.wvContent.goBack();
//                return true;
//            }
//        } else {
//            moveTaskToBack(true);
//        }
//        return super.onKeyDown(keyCode, event);
//    }


    public SmartEditText getSearchView2() {
        return searchView2;
    }

    public void setSearchView2(SmartEditText searchView2) {
        this.searchView2 = searchView2;
    }

    public void changeAvatar(Bitmap b) {
        if (b != null) {
            CircleImageView ivAvatar = (CircleImageView) menu.findViewById(R.id.iv_avatar);
            ivAvatar.setImageBitmap(b);
        }
    }


}
