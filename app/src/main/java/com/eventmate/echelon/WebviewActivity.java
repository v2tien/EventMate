package com.eventmate.echelon;

import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by ADMIN on 3/29/2016.
 */

public class WebviewActivity extends BaseActivity {
    public static final String KEY_URL = "URL";
    public static final String KEY_TITLE = "KEY_TITLE";

    private WebView wv;
    private String url;
    private String title;


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_session_qa;
    }

    @Override
    protected void initView() {
        getExtraData();

        initToolbar(title);

        loadWebView();
    }

    private void loadWebView(){
        wv = (WebView) findViewById(R.id.wb_content);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setDomStorageEnabled(true);
        wv.getSettings().setUseWideViewPort(true);
        wv.getSettings().setLoadWithOverviewMode(true);
        wv.setWebViewClient(new MyBrowser());
        wv.loadUrl(url);
    }

    private void getExtraData(){
        if (getIntent().hasExtra(KEY_URL) || getIntent().hasExtra(KEY_TITLE)){
            url = getIntent().getStringExtra(KEY_URL);
            title = getIntent().getStringExtra(KEY_TITLE);
        }else finish();
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && wv.canGoBack()) {
            Log.e("onKeyDown", "true");
            wv.goBack();
            return true;
        } else {
            Log.e("false", "false");
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

//    @Override
//    public void onBackPressed() {
//        if(wv != null) {
//            if (wv.canGoBack()) {
//                Log.e("onBackPressed", "true");
//                wv.goBack();
//            } else {
//                super.onBackPressed();
//            }
//        }
//    }
}
