package com.eventmate.echelon;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.eventmate.fragments.AgendaFragment;
import com.eventmate.fragments.SessionDetailFragment;
import com.eventmate.fragments.SessionQAFragment;
import com.eventmate.manager.ApiResponse;
import com.eventmate.manager.DataStore;
import com.eventmate.models.Session;
import com.eventmate.service.AlarmReceiver;
import com.eventmate.utils.ConnectionUtils;
import com.eventmate.utils.Constant;
import com.eventmate.utils.DateTimeUtils;
import com.eventmate.utils.LogUtils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import cz.msebera.android.httpclient.Header;

public class SessionDetailActivity extends BaseActivity implements View.OnClickListener {

    public static final String KEY_SESSION_ID = "SESSION_ID";
    public static final String KEY_TOKEN = "TOKEN";
    private Button btnDetail, btnQA;
    private ViewPager viewPager;
    private TextView tvTitle;
    private ImageView btnNotify,btnBookmark, btnBack;
    private Session mSession;
    private SessionDetailFragment mDetailFragment;
    private SessionQAFragment mQAFragment;

    private String mSessionId ;
    private String mToken;
    private boolean mNotifyState;
    private boolean mBookmarkState;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBgButton(0);
        getExtraData();
        getStatusSettings();
        getSessionDetail();

    }

    private void getStatusSettings() {
        // for notify
        mNotifyState = DataStore.getInstance().getNotifySetting(mSessionId);
        if (mNotifyState == true){
            btnNotify.setImageResource(R.drawable.ic_ring_white1);
        }else {
            btnNotify.setImageResource(R.drawable.ic_ring_unselected1);
        }

        // for bookmark
        //mBookmarkState = DataStore.getInstance().getBookmark(mSessionId);
        mBookmarkState = getIntent().getBooleanExtra(Constant.Key.KEY_IS_BOOKMARK, false);
        if (mBookmarkState == true){
            btnBookmark.setImageResource(R.drawable.ic_true_white1);
        }else {
            btnBookmark.setImageResource(R.drawable.ic_true_white_unselected);
        }
    }

    private void getExtraData() {
        if (getIntent().hasExtra(KEY_SESSION_ID)){
            mSessionId = getIntent().getStringExtra(KEY_SESSION_ID);
        }
        if (getIntent().hasExtra(KEY_TOKEN)){
            mToken = getIntent().getStringExtra(KEY_TOKEN);
        }
    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_session_detail;
    }

    @Override
    protected void initView() {
        initViews();
    }

    @Override
    public void onClick(View v) {
        if (v == btnDetail){
            setBgButton(0);
        }else if (v == btnQA){
            setBgButton(1);
        }else if (v == btnBack){
            finish();
        }else if (v == btnNotify){
            saveStatusNotify();
        }else if (v == btnBookmark){
            saveStatusBookmark();
        }
    }

    private void saveStatusBookmark() {
        if (mBookmarkState == true){
            unbookmark();
        }else {
            bookmark();
        }
    }

    private void unbookmark(){
        if(ConnectionUtils.checkConnection(this)){
            showProgressDialog("Loading...");
            JsonHttpResponseHandler handler = new JsonHttpResponseHandler(){
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    hideProgressDialog();
                    LogUtils.logInfo("Unbookmark:" + response.toString());
                    try {
                        boolean success = response.getBoolean(Constant.Key.KEY_SUCCESS);
                        String message = response.getString(Constant.Key.KEY_MESSAGE);
                        if(!success) {
                            showAlertMessage(message);
                        }
                        else if(success){
                            btnBookmark.setImageResource(R.drawable.ic_true_white_unselected);
                            mBookmarkState = false;
                            DataStore.getInstance().saveBookmark(mSessionId, mBookmarkState);
                            AgendaFragment.updateAdapter = true;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    hideProgressDialog();
                    ConnectionUtils.logOut(SessionDetailActivity.this);
                }
            };
            if(mToken!=null && !mToken.isEmpty()){
                ConnectionUtils.unBookmarkSession(mToken, mSession, handler);
            }else {
                ConnectionUtils.unBookmarkSession(EchelonApplication.getInstance().getToken(), mSession, handler);
            }
        }else{
            makeToast(R.string.network_no);
        }
    }

    private void bookmark(){
        if(ConnectionUtils.checkConnection(this)){
            showProgressDialog("Loading...");
            JsonHttpResponseHandler handler = new JsonHttpResponseHandler(){
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    hideProgressDialog();
                    try {
                        boolean success = response.getBoolean(Constant.Key.KEY_SUCCESS);
                        String message = response.getString(Constant.Key.KEY_MESSAGE);
                        if(!success) {
                            showAlertMessage(message);
                        }else
                        if(success){
                            btnBookmark.setImageResource(R.drawable.ic_true_white1);
                            mBookmarkState = true;
                            DataStore.getInstance().saveBookmark(mSessionId, mBookmarkState);
                            AgendaFragment.updateAdapter = true;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    hideProgressDialog();
                    ConnectionUtils.logOut(SessionDetailActivity.this);
                }
            };
            if(mToken!=null && !mToken.isEmpty()){
                ConnectionUtils.bookmark(mToken, Integer.toString(2), mSessionId, handler);
            }else {
                ConnectionUtils.bookmark(EchelonApplication.getInstance().getToken(), Integer.toString(2), mSessionId, handler);
            }
        }else{
            makeToast(R.string.network_no);
        }
    }

    private void saveStatusNotify() {
        if (mNotifyState == true){
            btnNotify.setImageResource(R.drawable.ic_ring_unselected1);
            mNotifyState = false;
            DataStore.getInstance().saveNotify(mSessionId,mNotifyState);
            cancelAlarmNotify();
        }else {
            btnNotify.setImageResource(R.drawable.ic_ring_white1);
            mNotifyState = true;
            DataStore.getInstance().saveNotify(mSessionId,mNotifyState);
            setNotify();
        }
    }

    private void initViews() {
        btnBack = (ImageView) findViewById(R.id.btn_back);

        btnDetail = (Button) findViewById(R.id.btn_detail);
        btnQA = (Button) findViewById(R.id.btn_qa);
        btnNotify = (ImageView) findViewById(R.id.btn_notify);
        btnBookmark = (ImageView) findViewById(R.id.btn_true);
        tvTitle = (TextView) findViewById(R.id.tv_title);

        viewPager = (ViewPager) findViewById(R.id.view_pagger);
        viewPager.setAdapter(new ViewpaggerAdapter(getSupportFragmentManager()));

        btnDetail.setOnClickListener(this);
        btnQA.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        btnNotify.setOnClickListener(this);
        btnBookmark.setOnClickListener(this);

    }

    private void setBgButton(int type){
        if (type == 0){
            btnDetail.setBackgroundResource(R.drawable.bg_session_button_detail_seleted);
            btnQA.setBackgroundResource(R.drawable.bg_session_button_qa_unselected);
            btnQA.setTextColor(ContextCompat.getColor(this, R.color.white));
            btnDetail.setTextColor(ContextCompat.getColor(this, R.color.session_text_color_button));
            viewPager.setCurrentItem(0);
        }else{
            btnQA.setBackgroundResource(R.drawable.bg_sesson_button_qa_selected);
            btnDetail.setBackgroundResource(R.drawable.bg_session_button_detail_unselected);
            btnDetail.setTextColor(ContextCompat.getColor(this, R.color.white));
            btnQA.setTextColor(ContextCompat.getColor(this, R.color.session_text_color_button));
            viewPager.setCurrentItem(1);

        }
    }

    private void getSessionDetail(){
        if (!ConnectionUtils.checkConnection(this)){
            showToast(R.string.network_no);
            return;
        }

        showProgressDialog(R.string.loading);
        RequestParams params = new RequestParams();
        params.put(Constant.Key.KEY_SESSION_ID, mSessionId);
        if(mToken!=null && !mToken.isEmpty()){
            params.put(Constant.Key.KEY_TOKEN, mToken);
        }else {
            params.put(Constant.Key.KEY_TOKEN, EchelonApplication.getInstance().getToken());
        }

        ConnectionUtils.get(Constant.Url.URL_SESSION_GET, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                hideProgressDialog();
                ApiResponse apiResponse =  new ApiResponse(response);
                if (!apiResponse.isError()) {
                    mSession = apiResponse.getResponseObject(Session.class);
                    bindData();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                hideProgressDialog();
                ConnectionUtils.logOut(SessionDetailActivity.this);
                super.onFailure(statusCode, headers, responseString, throwable);
            }
        });
    }

    private void bindData() {
        if (mSession != null){
            tvTitle.setText(mSession.getTitle());
            if (mDetailFragment != null)
                 mDetailFragment.setData(mSession);
        }
    }

    private void setNotify() {
        Intent intent = new Intent(this, AlarmReceiver.class);
        intent.putExtra(KEY_SESSION_ID, mSessionId);
        if(mToken!=null && !mToken.isEmpty()){
            intent.putExtra(KEY_TOKEN, mToken);
        }
        if(EchelonApplication.getInstance().getToken()!=null && !EchelonApplication.getInstance().getToken().isEmpty()){
            intent.putExtra(KEY_TOKEN, EchelonApplication.getInstance().getToken());
        }
        PendingIntent sender = PendingIntent.getBroadcast(this, Integer.parseInt(mSessionId), intent, 0);

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtils.FORMAT_DATETIME);
        try {
            calendar.setTime(sdf.parse(mSession.getStart_at()));// all done
           // calendar.setTime(sdf.parse("2016-03-28 12:10:00"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        // Schedule the alarm!
        AlarmManager am = (AlarmManager)getSystemService(ALARM_SERVICE);
        am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), sender);
        showToast("Notify on");
    }

    private void cancelAlarmNotify(){
        AlarmManager am = (AlarmManager)getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(this, AlarmReceiver.class);
        PendingIntent sender  = PendingIntent.getBroadcast(this, Integer.parseInt(mSessionId), intent, 0);
        am.cancel(sender);
    }

    private class ViewpaggerAdapter extends FragmentPagerAdapter {

        public ViewpaggerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0){
                mDetailFragment  = SessionDetailFragment.newInstance();
                return mDetailFragment;
            }else
                // TODO: 3/28/2016 Need remove google.com 
                //mQAFragment = SessionQAFragment.newInstance("http://www.google.com");
                mQAFragment = SessionQAFragment.newInstance("http://hotqa.com/#/eventmate/"+EchelonApplication.getInstance().getCode().trim()+"/"+mSessionId);
                return mQAFragment;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

}
