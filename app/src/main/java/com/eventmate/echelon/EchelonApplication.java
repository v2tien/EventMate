package com.eventmate.echelon;

import android.content.Intent;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.azstack.AzOptions;
import com.azstack.AzStackClient;
import com.azstack.AzUI;
import com.azstack.common.Utils;
import com.azstack.exception.AzStackException;
import com.azstack.listener.AzStackAppStatusListener;
import com.azstack.listener.AzStackConnectListener;
import com.azstack.listener.AzStackUserListener;
import com.eventmate.manager.DataStore;
import com.eventmate.models.Annoucement;
import com.eventmate.models.ContactType;
import com.eventmate.models.Event;
import com.eventmate.models.User;
import com.eventmate.service.NotifyService;
import com.eventmate.utils.Common;
import com.eventmate.utils.Constant;
import com.google.gson.Gson;
import com.loopj.android.http.HttpGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

public class EchelonApplication extends MultiDexApplication {

    public static EchelonApplication INSTANCE;
    public static int navigationHeight;
    public static int screenWidth, screenHeight;
    public static String userName = "";
    public static HomeActivity homeActivity;
    public static Gson gson;

    private String token; // token for whole app
    private Event event; // event seleted
    private User user; // event seleted

    private ArrayList<Annoucement> listAnnoucement = new ArrayList<Annoucement>();
    private Map<Integer, User> contacts = new HashMap<Integer, User>();

    private String code;


    /**
     * Store list event if user's code has not a token key
     *  Using for #EventChoseActivity
     */
    private List<Event> listEvent;
    private List<User> listUser;

    private ArrayList<ContactType> listContactType = new ArrayList<ContactType>();

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        Common.context = getApplicationContext();
        DataStore.init(this);
        gson = new Gson();

        Log.e("package", Common.context.getPackageName()+"");
        if (Common.userId > 0) {
            initAzStack();
        }
    }

    public static EchelonApplication getInstance(){
        return INSTANCE;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public User getUser(int userId) {
        User contact = contacts.get(userId);
        return contact;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Event getEvent() {
        return event;
    }


    public void setEvent(Event event) {
        this.event = event;
    }

    public ArrayList<ContactType> getListContactType() {
        return listContactType;
    }

    public void setListContactType(ArrayList<ContactType> listContactType) {
        this.listContactType = listContactType;
    }


    public List<Event> getListEvent() {
        if (listEvent == null)
            listEvent = new ArrayList<>();
        return listEvent;
    }

    public void setListEvent(List<Event> listEvent) {
        this.listEvent = listEvent;
    }

    public List<User> getListUser() {
        if (listUser == null)
            listUser = new ArrayList<>();
        return listUser;
    }

    public void setListUser(List<User> listUser) {
        this.listUser = listUser;
    }

    public  ArrayList<Annoucement> getListAnnoucement() {
        return listAnnoucement;
    }

    public  void setListAnnoucement(ArrayList<Annoucement> listAnnoucement) {
        this.listAnnoucement = listAnnoucement;
    }
    /*
    Connect SDK
    */
    public static void initAzStack() {
        AzUI azUI = AzUI.getInstance(Common.context);

        azUI.setHeaderColor(0xffdd5555);
        azUI.setNotificationIcon(R.drawable.notify_logo);

        AzOptions azOptions = new AzOptions();
        azOptions.setGoogleCloudMessagingId(Common.senderId);

        AzStackClient azStackClient = AzStackClient.newInstance(Common.context, Common.appId, Common.publicKey, azOptions);
//        Intent intent = new Intent(Common.context, HomeActivity.class);
//        azStackClient.enableSingleNotification(intent, Common.context.getString(R.string.app_name));
//        azStackClient.setAppName(Common.context.getString(R.string.app_name));
        getInstance().connectAzstackClient(azStackClient);
    }

    public void connectAzstackClient(AzStackClient azStackClient){
        azStackClient.registerConnectionListenter(new AzStackConnectListener() {

            @Override
            public void onConnectionError(AzStackClient client, AzStackException e) {
                Log.e("Error", "error");
            }

            @Override
            public void onConnectionDisconnected(AzStackClient client) {
                Log.e("Disconnect", "Disconnect");
            }

            @Override
            public void onConnectionConnected(AzStackClient client) {
                NotifyService.notifyAppStatus(Constant.Key.APP_STATUS_CONNECTED);
                Common.appStatus = Constant.Key.APP_STATUS_CONNECTED;
                Log.e("Connected", "connected");
            }
        });

        azStackClient.registerAppStatusListener(new AzStackAppStatusListener() {
            @Override
            public void onAppStatus(int status, String messageStatus) {
                Common.appStatus = status;
                NotifyService.notifyAppStatus(status);
                Utils.log("+++++++++++++++++++onAppStatus=" + status + "--mesageStatus=" + messageStatus);
            }
        });

        azStackClient.registerUserListener(new AzStackUserListener() {

            @Override
            public void getUserInfo(List<String> azStackUserIds, int purpose) {
                final JSONArray arrayContact = new JSONArray();
                List<User> users = getInstance().getListUser();
                Log.e("List<String>", azStackUserIds.toString()+"");
                for (final String azStackUserId : azStackUserIds) {
                    // need get your app's user info
                    if(users.size() >0 ){
                        for (int i = 0; i< users.size(); i++){
                            JSONObject jsonContact = new JSONObject();
                            if(users.get(i).getContact_id().equals(azStackUserId) &&
                                    Integer.parseInt(users.get(i).getContact_id()) != Common.userId){
                                Log.e("id",users.get(i).getContact_id() +"--"+ azStackUserId);
                                try {
                                    jsonContact.put("azStackUserId", azStackUserId);
                                    jsonContact.put("name", users.get(i).getFirst_name() + " " + users.get(i).getLast_name());
                                    jsonContact.put("avatar", users.get(i).getImg());
                                    jsonContact.put("phoneNumber", users.get(i).getCell_phone());
                                    arrayContact.put(jsonContact);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }else{
                        if(Integer.parseInt(azStackUserId) != Common.userId) {
                            getInstance().getUseInfoById(azStackUserId, arrayContact);
                        }
                    }
                }
                AzStackClient.getInstance().getUserInfoComplete(arrayContact, purpose);
            }

            @Override
            public void viewUserInfo(String appUserId) {
                List<User> users = getInstance().getListUser();
                try {
                    int userId = Integer.valueOf(appUserId);
                    if (userId == Common.userId) {
//                        Intent intent = new Intent(Common.context, MyProfileActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        Common.context.startActivity(intent);
                    } else {
                        for(int i = 0; i< users.size(); i++){
                            if(Integer.parseInt(users.get(i).getContact_id()) == userId &&
                                    userId != Common.userId){
                                Log.e("id info",users.get(i).getContact_id() +"-"+ userId);
                                Intent intent = new Intent(Common.context, ContactActivity.class);
                                intent.putExtra("userId", userId);
                                intent.putExtra(ContactActivity.PREF_USER_KEY, users.get(i));
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                Common.context.startActivity(intent);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public JSONArray getListFriend() {
                // truyen vao list contact
                List<User> users = getInstance().getListUser();
                JSONArray arrayContact = new JSONArray();
                for (User user : users) {
                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("name", user.getFirst_name() +" "+ user.getLast_name());
                        jsonObject.put("avatar", user.getImg());
                        jsonObject.put("azStackUserId", user.getContact_id());
                        jsonObject.put("phoneNumber", user.getCell_phone());
                        arrayContact.put(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                return arrayContact;
            }
        });
        AzStackClient.getInstance().setAzStackAccount(Common.userId + "", "", Common.name);
//        azStackClient.connect(Common.userId + "", Common.name, Common.name);
    }
    public void clearData(){
        event = null;
        token = null;
        event = null;
        listContactType.clear();
        listAnnoucement.clear();
    }

    public static void connect() {
        if (AzStackClient.getInstance() != null)
            AzStackClient.getInstance().connectIfDiconnect();
    }

    public void getUseInfoById(String userId, JSONArray arrayContact){
        String url = "http://api.eventmateapp.com/api/contact/get?"+"token=" +EchelonApplication
                .getInstance().getToken()+"&"+"contact_id="+userId;
        Log.e("url", url);
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);
        HttpResponse response;
        try {
            response = httpclient.execute(httpget);
            // Get hold of the response entity
            HttpEntity entity = response.getEntity();
            // If the response does not enclose an entity, there is no need
            // to worry about connection release
            if (entity != null) {
                // A Simple JSON Response Read
                InputStream instream = entity.getContent();
                String result = convertStreamToString(instream);
                JSONObject jsonOb = new JSONObject(result);
                JSONObject jsonContact = new JSONObject();

                JSONObject obData = jsonOb.getJSONObject("data");
                Log.e("data", obData.toString());
//                try {
                    jsonContact.put("azStackUserId", userId);
                    jsonContact.put("name", obData.getString("first_name")+" "+ obData.getString("last_name"));
                    jsonContact.put("avatar", obData.getString("img"));
                    jsonContact.put("phoneNumber", obData.getString("cell_phone"));
                    arrayContact.put(jsonContact);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
                // now you have the string representation of the HTML request
                instream.close();
            }
        } catch (Exception e) {}
    }
    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
