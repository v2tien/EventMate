package com.eventmate.echelon;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.azstack.AzStackClient;
import com.eventmate.models.User;
import com.squareup.picasso.Picasso;

/**
 * Created by Trang on 3/24/2016.
 */
public class ContactActivity extends BaseActivity implements View.OnClickListener {

    public static final String PREF_USER_KEY = "PREF_USER_KEY";

    private User mUser;
    private Toolbar toolbar;
    private ImageView btnFace, btnTwitter, btnLinkedIn, btnWeb, imgAvartar;
    private LinearLayout btnChat, btnEmail, btnCall;
    private TextView tvContent, tvName, tvPosition, tvAvartar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getExtraData();
        setData();
    }

    private void getExtraData() {
        if (getIntent().hasExtra(PREF_USER_KEY)){
            mUser = (User) getIntent().getSerializableExtra(PREF_USER_KEY);
            if(mUser.getUrl_facebook() == null || mUser.getUrl_facebook().isEmpty()){
                btnFace.setVisibility(View.GONE);
            }
            if(mUser.getUrl_linkedin() == null || mUser.getUrl_linkedin().isEmpty()){
                btnLinkedIn.setVisibility(View.GONE);
            }
            if(mUser.getUrl_twitter() == null || mUser.getUrl_twitter().isEmpty()){
                btnTwitter.setVisibility(View.GONE);
            }
            if(mUser.getInformation_url() == null || mUser.getInformation_url().isEmpty()){
                btnWeb.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_contact_detail;
    }

    @Override
    protected void initView() {
        initToolbar();

        btnChat = (LinearLayout) findViewById(R.id.btn_chat);
        btnCall = (LinearLayout) findViewById(R.id.btn_call);
        btnEmail = (LinearLayout) findViewById(R.id.btn_email);
        tvAvartar = (TextView) findViewById(R.id.tv_avatar);

        btnFace = (ImageView) findViewById(R.id.btn_face);
        btnTwitter = (ImageView) findViewById(R.id.btn_twitter);
        btnLinkedIn = (ImageView) findViewById(R.id.btn_linkedin);
        btnWeb = (ImageView) findViewById(R.id.btn_web);
        imgAvartar = (ImageView) findViewById(R.id.img_avartar);
        tvContent = (TextView) findViewById(R.id.tv_content);
        tvName = (TextView) findViewById(R.id.tv_username);
        tvPosition = (TextView) findViewById(R.id.tv_position);

        btnCall.setOnClickListener(this);
        btnChat.setOnClickListener(this);
        btnEmail.setOnClickListener(this);

        btnFace.setOnClickListener(this);
        btnTwitter.setOnClickListener(this);
        btnLinkedIn.setOnClickListener(this);
        btnWeb.setOnClickListener(this);
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        showAlertMessage("clicked");
        return true;
    }
*/

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case  R.id.btn_chat:
                if(mUser != null) {
                    AzStackClient.getInstance().startChat(ContactActivity.this, "" + mUser.getContact_id(),
                            mUser.getFirst_name()+" "+mUser.getLast_name(), mUser.getImg());
                }

            break;

            case  R.id.btn_call:
                if (mUser != null) {
                    AzStackClient.getInstance().startCall(ContactActivity.this, "" + mUser.getContact_id(),
                            mUser.getFirst_name()+" "+mUser.getLast_name() , mUser.getImg());
                }
                break;

            case  R.id.btn_email:
                sendMail();
                break;
            case  R.id.btn_face:
                openWebpage(mUser.getUrl_facebook());
                break;
            case  R.id.btn_twitter:
                openWebpage(mUser.getUrl_twitter());
                break;
            case  R.id.btn_linkedin:
                openWebpage(mUser.getUrl_linkedin());
                break;
            case  R.id.btn_web:
                openWebpage(mUser.getWebsite());
                break;
        }
    }

    private void sendMail() {
        String[] TO = {mUser.getEmail()};
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setData(Uri.parse("mailto:"));
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, TO);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Your Subject");
        intent.putExtra(Intent.EXTRA_TEXT, "Your body");

        startActivity(Intent.createChooser(intent, "Send Email"));
    }

    private void initToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.showOverflowMenu();
        //toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void setData(){
        if (mUser != null){
            getSupportActionBar().setTitle(mUser.getFirst_name()+" "+mUser.getLast_name());
            tvName.setText(mUser.getFirst_name()+" "+mUser.getLast_name());
            tvContent.setText("Email: "+mUser.getEmail());
            tvPosition.setText(mUser.getJob_title());
            if (mUser.getImg() != null){
                Picasso.with(this).load(mUser.getImg()).into(imgAvartar);
                tvAvartar.setVisibility(View.INVISIBLE);
            }else {
                tvAvartar.setVisibility(View.VISIBLE);
                imgAvartar.setVisibility(View.INVISIBLE);
                tvAvartar.setText(Character.toString(mUser.getFirst_name().charAt(0)).toUpperCase() + Character.toString(mUser.getLast_name().charAt(0)).toUpperCase());
            }
        }
    }
}
