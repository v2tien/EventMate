package com.eventmate.echelon;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.eventmate.adapter.AnnoucementAdapter;
import com.eventmate.impl.IAdapterListenner;
import com.eventmate.models.Annoucement;
import com.eventmate.utils.ConnectionUtils;
import com.eventmate.utils.Constant;
import com.eventmate.utils.LogUtils;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class AnnoucementActivity extends BaseActivity implements View.OnClickListener, IAdapterListenner{

    RecyclerView rv;
    ImageView btReset, btBack;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_annoucement;
    }

    @Override
    protected void initView() {
        rv = (RecyclerView)findViewById(R.id.rv);
        //init data
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        AnnoucementAdapter adapter = new AnnoucementAdapter(EchelonApplication.getInstance().getListAnnoucement(), this);
        rv.setAdapter(adapter);
        btReset = (ImageView)findViewById(R.id.bt_reset);
        btReset.setOnClickListener(this);
        btBack = (ImageView)findViewById(R.id.bt_action);
        btBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_reset:
                getListAnnouncement();
                break;
            case R.id.bt_action:
                finish();
                break;
        }
    }

    private void getListAnnouncement(){


        JsonHttpResponseHandler handler = new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                hideProgressDialog();
                LogUtils.logInfo("Annou data:"+response.toString());
                try {
                    boolean success = response.getBoolean(Constant.Key.KEY_SUCCESS);
                    if(success){
                        Gson gson = new Gson();
                        JSONArray arrData = response.getJSONArray(Constant.Key.KEY_DATA);
                        for(int i=0;i<arrData.length();i++){
                            Annoucement annoucement = gson.fromJson(arrData.getJSONObject(i).toString(), Annoucement.class);
                            EchelonApplication.getInstance().getListAnnoucement().add(annoucement);
                        }
                        AnnoucementAdapter adapter = new AnnoucementAdapter(EchelonApplication.getInstance().getListAnnoucement(), AnnoucementActivity.this);
                        rv.setAdapter(adapter);
                        LogUtils.logInfo("NUmber anno:" + EchelonApplication.getInstance().getListAnnoucement().size());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                hideProgressDialog();
                ConnectionUtils.logOut(AnnoucementActivity.this);
            }
        };

            if(ConnectionUtils.checkConnection(this)){
                showProgressDialog("Loading...");
                EchelonApplication.getInstance().getListAnnoucement().clear();
                ConnectionUtils.getListAnnouncement(EchelonApplication.getInstance().getToken(), handler);
            }else{
                makeToast(R.string.network_no);
            }


    }

    @Override
    public void onClickItemAdapter(int position) {
        LogUtils.logInfo("annou webview");
        Intent intent = new Intent(this, DVWebviewActivity.class);
        intent.putExtra(WebviewActivity.KEY_URL, EchelonApplication.getInstance().getListAnnoucement().get(position).getUrl());

        startActivity(intent);

    }
}

