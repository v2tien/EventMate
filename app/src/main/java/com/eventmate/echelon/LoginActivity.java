package com.eventmate.echelon;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eventmate.manager.ApiResponse;
import com.eventmate.manager.DataStore;
import com.eventmate.models.Contact;
import com.eventmate.models.Event;
import com.eventmate.models.User;
import com.eventmate.service.Notify;
import com.eventmate.utils.Common;
import com.eventmate.utils.ConnectionUtils;
import com.eventmate.utils.Constant;
import com.eventmate.utils.LogUtils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private RelativeLayout loRoot;
    private EditText edtCode;
    private TextView tvForget;
    private BroadcastReceiver loginReceiver;
    private LinearLayout btnLoginFb;
    private CallbackManager callbackManager;
    private Contact contact;

    @Override
    protected int getLayoutId() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        return R.layout.activity_login;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkLoginData();
    }

    private final FacebookCallback<LoginResult> fbCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            getUserFacebookData(loginResult.getAccessToken());
            Log.e("onSuccess", "onSuccess");
        }

        @Override
        public void onCancel() {
            Log.e("cancel", "cancel");
        }

        @Override
        public void onError(FacebookException e) {
            Log.e("error", "error");
        }
    };

    private void loginFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this,
                Arrays.asList("public_profile", "email"));
        LoginManager.getInstance()
                .registerCallback(callbackManager, fbCallback);
        LoginManager.getInstance().logOut();
        if (AccessToken.getCurrentAccessToken() != null) {
            getUserFacebookData(AccessToken.getCurrentAccessToken());
        }
    }

    private void getUserFacebookData(final AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                contact = new Gson().fromJson(jsonObject.toString(), Contact.class);
                Log.e("email=", contact.getEmail() + "");
                try {
                    contact.setImg(jsonObject.getJSONObject("picture")
                            .getJSONObject("data").getString("url"));
                    String id = jsonObject.getString("id");
                    String url_fb = "http://facebook.com/" + id;

                    getDataFb(contact.getFirst_name(), contact.getLast_name(), contact.getEmail(), url_fb, contact.getImg());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields",
                "id,first_name,last_name,email,picture.type(large)");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void getDataFb(String firstName, String lastName, String email, String facebook, String avatar) {
        if (email != null) {
            JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Log.e("update info", response.toString());
//                hideProgressDialog();
                    try {
                        boolean success = response.getBoolean(Constant.Key.KEY_SUCCESS);
//                    String message = response.getString(Constant.Key.KEY_MESSAGE);

                        if (success) {
                            JSONObject objNewContact = response.getJSONObject("new_contact");
                            String code = objNewContact.getString("code");
                            Log.e("code=", code);
                            if (code.length() > 0) {
                                checkEventCode(code);
                            }
                        } else {
                            Toast.makeText(LoginActivity.this, "Login fail, please try again!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    hideProgressDialog();
                    ConnectionUtils.logOut(LoginActivity.this);
                }
            };

            if (!ConnectionUtils.checkConnection(this)) {
                showAlertMessage(R.string.network_no);
            } else {
                showProgressDialog(R.string.loading);
                ConnectionUtils.loginFB(firstName, lastName, email, facebook, avatar, handler);
            }
        } else {
            Toast.makeText(LoginActivity.this, "Invalid email address", Toast.LENGTH_SHORT).show();
//            return;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    private void checkLoginData() {
        String loginData = DataStore.getInstance().getLoginData();
        String code = DataStore.getInstance().getLoginCode();
        LogUtils.logInfo("login data:" + loginData);
        if (!loginData.isEmpty()) {
            try {
                JSONObject response = new JSONObject(loginData);

                ApiResponse apiResponse = new ApiResponse(response);
                if (!apiResponse.isError()) {
                    try {
                        saveLoginInfo(response);
                        saveLoginCode(code);
                        String token = response.getString(Constant.Key.KEY_TOKEN);
                        LogUtils.logInfo("Token:" + token);
                        if (!token.equals(Constant.Key.KEY_FALSE)) {
                            // save user

                            List<User> users = apiResponse.getResponseObjectList(User.class);
                            User user = users.get(0);
                            EchelonApplication.getInstance().setUser(user);
                            //DataStore.getInstance().saveUser(user);
                            // save token
                            EchelonApplication.getInstance().setToken(token);
                            //login sdk
                            Common.token = token;
                            Common.userId = Integer.parseInt(user.getContact_id());
                            Common.name = user.getFirst_name() + " " + user.getLast_name();

                            EchelonApplication.initAzStack();
                            EchelonApplication.getInstance().setCode(code);
                            gotoActivity(HomeActivity.class);
                            finish();
                        } else {
                            LogUtils.logInfo("token false");
                            if (response.has("contact")) {
                                JSONObject object = response.getJSONObject("contact");
                                LogUtils.logInfo("has contact:" + object.toString());
                                EchelonApplication.userName = object.getString("first_name") + " " + object.getString("last_name");
                                DataStore.getInstance().saveUserName(EchelonApplication.userName);
                            } else {
                                LogUtils.logInfo("Not have contact data");
                            }
                            List<Event> events = apiResponse.getResponseObjectList(Event.class);
                            EchelonApplication.getInstance().setListEvent(events);
                            EchelonApplication.getInstance().setCode(code);
                            gotoSelectEvent(code);


                        }
                        finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gotoActivity(HomeActivity.class);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void initView() {
        edtCode = (EditText) findViewById(R.id.edt_code);
        tvForget = (TextView) findViewById(R.id.tv_forget_pass);
        loRoot = (RelativeLayout) findViewById(R.id.root_view);
        btnLoginFb = (LinearLayout) findViewById(R.id.view_login_fb);
        btnLoginFb.setVisibility(View.VISIBLE);

        btnLoginFb.setOnClickListener(this);
        loRoot.setOnClickListener(this);

        tvForget.setOnClickListener(this);

        edtCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO
                        || actionId == EditorInfo.IME_ACTION_DONE) {
                    String code = edtCode.getText().toString();
                    if (code.length() == 0)
                        return false;
                    checkEventCode(code);
                    //gotoActivity(SessionDetailActivity.class);
                    return true;
                }
                return false;
            }
        });
/*edtCode.setText("gg981a");
        edtCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkEventCode(edtCode.getText().toString());
            }
        });*/
    }

    private void checkEventCode(final String code) {
        if (!ConnectionUtils.checkConnection(this)) {
            showToast(R.string.network_no);
            return;
        }
//        showProgressDialog(R.string.loading);
        ConnectionUtils.login(code, "", new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                hideProgressDialog();
                ApiResponse apiResponse = new ApiResponse(response);
                Log.e("check code", response.toString());
                if (!apiResponse.isError()) {
                    try {
                        saveLoginInfo(response);
                        saveLoginCode(code);
                        String token = response.getString(Constant.Key.KEY_TOKEN);
                        LogUtils.logInfo("Token:" + token);
                        if (!token.equals(Constant.Key.KEY_FALSE)) {
                            // save user

                            List<User> users = apiResponse.getResponseObjectList(User.class);
                            User user = users.get(0);
                            EchelonApplication.getInstance().setUser(user);
                            //DataStore.getInstance().saveUser(user);
                            // save token
                            EchelonApplication.getInstance().setToken(token);
                            //login sdk
                            Common.token = token;
                            Common.userId = Integer.parseInt(user.getContact_id());
                            Common.name = user.getFirst_name() + " " + user.getLast_name();

                            EchelonApplication.initAzStack();
                            EchelonApplication.getInstance().setCode(code);
                            gotoActivity(HomeActivity.class);
                            finish();
                        } else {
                            LogUtils.logInfo("token false");
                            if (response.has("contact")) {
                                JSONObject object = response.getJSONObject("contact");
                                LogUtils.logInfo("has contact:" + object.toString());
                                EchelonApplication.userName = object.getString("first_name") + " " + object.getString("last_name");
                                DataStore.getInstance().saveUserName(EchelonApplication.userName);
                                DataStore.getInstance().setUserAvatar(object.getString("img"));
                            } else {
                                LogUtils.logInfo("Not have contact data");
                            }
                            List<Event> events = apiResponse.getResponseObjectList(Event.class);
                            EchelonApplication.getInstance().setListEvent(events);
                            EchelonApplication.getInstance().setCode(code);
                            gotoSelectEvent(code);
                        }
                        finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gotoActivity(HomeActivity.class);

                } else {
                    showAlertMessage(R.string.event_code_fail);
                }

            }


        });
    }

    private void gotoSelectEvent(String code) {
        Intent intent = new Intent(this, EventChoseActivity.class);
        intent.putExtra(EventChoseActivity.KEY_CODE_USER, code);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        if (v == tvForget) {
            gotoActivity(ForgetPasswordActivity.class);
        } else if (v == loRoot) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(edtCode.getWindowToken(), 0);
        } else if (v == btnLoginFb) {
            loginFacebook();
        }
    }

    private void registerBroadcastReceiver() {
        IntentFilter filter1 = new IntentFilter(Notify.LOGIN_SUCESS.getValue());
        loginReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                EchelonApplication.initAzStack();

                //di vao man hinh home
                Intent gotoHomeItent = new Intent(getBaseContext(), HomeActivity.class);
//                gotoHomeItent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(gotoHomeItent);

                finish();
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(loginReceiver, filter1);
    }

    private void unregisterBroadcastReceiver() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(loginReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
//        registerBroadcastReceiver();
    }

    @Override
    public void onPause() {
        super.onPause();
//        unregisterBroadcastReceiver();
    }

    private void saveLoginInfo(JSONObject response) {
        DataStore.getInstance().saveLoginData(response);
    }

    private void saveLoginCode(String code) {
        DataStore.getInstance().saveLoginCode(code);
    }

    /*private EditText edtCode;
    private Button btnSubmitCode;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initView() {
        edtCode = (EditText)findViewById(R.id.edt_code);
        btnSubmitCode = (Button)findViewById(R.id.btn_submit_code);
        btnSubmitCode.setOnClickListener(this);
    }

    private void goHomeScreen(){
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_submit_code:
                goHomeScreen();
                break;
        }
    }*/

}
