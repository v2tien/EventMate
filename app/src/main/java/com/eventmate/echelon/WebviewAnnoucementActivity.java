package com.eventmate.echelon;

import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by Trang on 3/30/2016.
 */
public class WebviewAnnoucementActivity extends BaseActivity {

    public static final String KEY_URL = "URL";

    private WebView wv;
    private String url;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_session_qa;
    }

    @Override
    protected void initView() {
        getExtraData();
        initToolbar("");
        loadWebView();

    }

    private void loadWebView() {

        wv = (WebView) findViewById(R.id.wb_content);

        wv.setWebViewClient(new MyBrowser());
        wv.loadUrl(url);
    }

    private void getExtraData(){
        if (getIntent().hasExtra(KEY_URL)){
            url = getIntent().getStringExtra(KEY_URL);
        }else finish();
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
