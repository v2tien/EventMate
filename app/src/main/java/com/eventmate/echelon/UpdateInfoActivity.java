package com.eventmate.echelon;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;

import com.eventmate.utils.ConnectionUtils;
import com.eventmate.utils.Constant;
import com.eventmate.utils.LogUtils;
import com.eventmate.utils.StringUtils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

public class UpdateInfoActivity extends BaseActivity implements View.OnClickListener {

    private static final int REQUEST_CODE = 69;

    private CircleImageView ivAvatar;
    private EditText etFirstName;
    private EditText etLastName;
    private EditText etMobile;
    private EditText etEmail;
    private EditText etCompany;
    private EditText etWebsite;
    private EditText etFacebook;
    private EditText etLinkedin;
    private EditText etTwitter;

    private Bitmap b;
    private String encodedString = "";
    private String imgExtension = "";

    @Override
    protected int getLayoutId() {
        return R.layout.activity_update_info;
    }

    @Override
    protected void initView() {
        ivAvatar = (CircleImageView) findViewById(R.id.iv_avatar);
        if(EchelonApplication.getInstance().getUser().getImg()!=null &&!EchelonApplication.getInstance().getUser().getImg().isEmpty()){
            Picasso.with(this).load(EchelonApplication.getInstance().getUser().getImg()).into(ivAvatar);
        }
        ivAvatar.setOnClickListener(this);
        etFirstName = (EditText) findViewById(R.id.etFirstName);
        etLastName = (EditText) findViewById(R.id.etLastName);
        etWebsite = (EditText) findViewById(R.id.etWebsite);
        etCompany = (EditText) findViewById(R.id.etCompany);
        etMobile = (EditText) findViewById(R.id.etMobile);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etFacebook = (EditText) findViewById(R.id.etFacebook);
        etTwitter = (EditText) findViewById(R.id.etTwitter);
        etLinkedin = (EditText) findViewById(R.id.etLinkedin);
        findViewById(R.id.bt_update).setOnClickListener(this);
        findViewById(R.id.bt_action).setOnClickListener(this);
    }

    private void getNewAvatar() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, REQUEST_CODE);
    }

    private String encodeBitmap(Bitmap b, Bitmap.CompressFormat compressFormat, int quality) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        b.compress(compressFormat, quality, baos);
        return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            try {
                InputStream is = getContentResolver().openInputStream(data.getData());
                b = BitmapFactory.decodeStream(is);
                is.close();
                encodedString = encodeBitmap(b, Bitmap.CompressFormat.JPEG, 100);
                imgExtension = getContentResolver().getType(data.getData());
                getExtension();
                LogUtils.logInfo("image extension:" + imgExtension);
                ivAvatar.setImageBitmap(b);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }



    private void validateData() {
        LogUtils.logInfo("Token="+EchelonApplication.getInstance().getToken()+"; contactId="+EchelonApplication.getInstance().getUser().getContact_id());
        String firstName = "", lastName = "", mobile = "", email = "", company = "", website = "", facebook = "", linkedin = "", twitter = "";
        firstName = etFirstName.getText().toString();
        lastName = etLastName.getText().toString();
        mobile = etMobile.getText().toString();
        email = etEmail.getText().toString();
        company = etCompany.getText().toString();
        website = etWebsite.getText().toString();
        facebook = etFacebook.getText().toString();
        twitter = etTwitter.getText().toString();
        linkedin = etLinkedin.getText().toString();

       /* if (!mobile.isEmpty() && !StringUtils.isValidPhoneNumber(mobile)) {
            etMobile.setError("Invalid phonenumber");
            return;
        }*/

        if (!email.isEmpty() && !StringUtils.isValidEmail(email)) {
            etEmail.setError("Invalid email address");
            return;
        }

        JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                LogUtils.logInfo("update info:"+response.toString());
                hideProgressDialog();
                try {
                    boolean success = response.getBoolean(Constant.Key.KEY_SUCCESS);
                    String message = response.getString(Constant.Key.KEY_MESSAGE);

                    if(success){
                        EchelonApplication.homeActivity.changeAvatar(b);
                    }
                    showAlertMessage(message);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                hideProgressDialog();
                ConnectionUtils.logOut(UpdateInfoActivity.this);
            }
        };

        if (!ConnectionUtils.checkConnection(this)) {
            showAlertMessage(R.string.network_no);
        } else {
            showProgressDialog("Updating...");
            ConnectionUtils.updateInfo(EchelonApplication.getInstance().getToken(), firstName, lastName, mobile, email, company, website, facebook, linkedin, twitter, encodedString, "", handler);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_avatar:
                getNewAvatar();
                break;
            case R.id.bt_update:
                validateData();
                break;
            case R.id.bt_action:
                finish();
                break;
        }
    }

    private void getExtension(){
        int offset = imgExtension.indexOf('/');
        imgExtension = imgExtension.substring(offset+1);
    }




}
