# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Develop\AndroidStudio\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-keepattributes Signature
-keepattributes *Annotation*
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.examples.android.model.** { *; }

-keep class com.android.support.** { *; }
-keep class com.google.** { *; }
-keep class com.eventmate.models.** { *; }
-keep class com.eventmate.echelon.** { *; }
-keep class de.hdodenhof.circleimageview** { *; }
-keep class com.loopj.android.** { *; }
-keep class org.webrtc.** { *; }
-dontwarn org.apache.**
-keep class org.apache.** { *; }
-dontwarn android.net.http.AndroidHttpClient
-dontwarn com.squareup.picasso.OkHttpDownloader